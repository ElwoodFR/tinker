<?php

namespace App\Http\Controllers;

use App\Task;
use App\Utility;
use Auth;

class CalenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $currantWorkspace = Utility::getWorkspaceBySlug($slug);
        if($currantWorkspace->permission == 'Owner'){
            $tasks            = Task::select('tasks.*')->join('projects', 'projects.id', '=', 'tasks.project_id')->where('projects.workspace', '=', $currantWorkspace->id)->get();
        }else{
            $tasks            = Task::select('tasks.*')->join('projects', 'projects.id', '=', 'tasks.project_id')->where('projects.workspace', '=', $currantWorkspace->id)->where('tasks.assign_to', '=', Auth::user()->id)->get();
        }

        $arrayJson        = [];
        foreach($tasks as $task)
        {
            $arrayJson[] = [
                "title" => $task->title,
                "start" => $task->start_date,
                "end" => $task->due_date,
                "url" => route(
                    'tasks.show', [
                                    $currantWorkspace->slug,
                                    $task->project_id,
                                    $task->id,
                                ]
                ),
                "task_id" => $task->id,
                "task_url" => route(
                    'tasks.drag.event', [
                                          $currantWorkspace->slug,
                                          $task->project_id,
                                          $task->id,
                                      ]
                ),
                "className" => ($task->priority == 'Low') ? 'bg-primary border-primary' : (($task->priority == 'Medium') ? 'bg-warning border-warning' : (($task->priority == 'High') ? 'bg-danger border-danger' : '')),
                "allDay" => true,
            ];
        }

        return view('calendar.index', compact('currantWorkspace', 'arrayJson'));
    }
}
