<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = [
        'title','text','workspace','color','created_by'
    ];

    public function user(){
        return $this->hasOne('App\User','id','created_by');
    }
}
