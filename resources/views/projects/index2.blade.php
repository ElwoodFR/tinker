@if((isset($permisions) && in_array('show activity',$permisions)) || $currantWorkspace->permission == 'Owner')
<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                        
    <div class="widget widget-activity-one">

        <div class="widget-heading">
            <h5 class="">{{ __('Activity') }}</h5>
        </div>

        <div class="widget-content">

            <div class="mt-container mx-auto ps ps--active-y">
                <div class="timeline-line">
                    @foreach($project->activities as $activity)
                    <div class="item-timeline timeline-new">
                        <p class="t-time">{{$activity->created_at->diffForHumans()}}</p>
                        <div class="t-dot" data-original-title="" title="">
                        </div>
                        <div class="t-text">
                            @if($activity->log_type == 'Move')
                            <div class="activity-icon bg-primary text-white shadow-primary">
                                <i class="mdi mdi-cursor-move"></i>
                            </div>
                        @elseif($activity->log_type == 'Create Milestone')
                            <div class="activity-icon bg-primary text-white shadow-primary">
                                <i class="mdi mdi-target"></i>
                            </div>
                        @elseif($activity->log_type == 'Create Task')
                            <div class="activity-icon bg-primary text-white shadow-primary">
                                <i class="mdi mdi-format-list-checks"></i>
                            </div>
                        @elseif($activity->log_type == 'Invite User')
                            <div class="activity-icon bg-primary text-white shadow-primary">
                                <i class="mdi mdi-plus"></i>
                            </div>
                        @elseif($activity->log_type == 'Share with Client')
                            <div class="activity-icon bg-primary text-white shadow-primary">
                                <i class="mdi mdi-plus"></i>
                            </div>
                        @elseif($activity->log_type == 'Upload File')
                            <div class="activity-icon bg-primary text-white shadow-primary">
                                <i class="mdi mdi-file"></i>
                            </div>
                        @elseif($activity->log_type == 'Create Timesheet')
                            <div class="activity-icon bg-primary text-white shadow-primary">
                                <i class="mdi mdi-clock"></i>
                            </div>
                        @elseif($activity->log_type == 'Create Bug')
                            <div class="activity-icon bg-primary text-white shadow-primary">
                                <i class="mdi mdi-bug"></i>
                            </div>
                        @elseif($activity->log_type == 'Move Bug')
                            <div class="activity-icon bg-primary text-white shadow-primary">
                                <i class="mdi mdi-cursor-move"></i>
                            </div>
                        @endif
                            <p><span>{!! $activity->getRemark() !!}</p>
                        </div>
                    </div>
                    @endforeach

                </div>  
            </div>                                  
        </div>
    </div>
</div>
@endif



@if((isset($permisions) && in_array('show activity',$permisions)) || $currantWorkspace->permission == 'Owner')
<div class="card card-primary">
    <div class="card-header">
        <h4>{{ __('Activity') }}</h4>
    </div>
    <div class="card-body pr-2 pl-3">
        <div class="activities top-10-scroll">
            @foreach($project->activities as $activity)
                <div class="activity">
                    @if($activity->log_type == 'Move')
                        <div class="activity-icon bg-primary text-white shadow-primary">
                            <i class="mdi mdi-cursor-move"></i>
                        </div>
                    @elseif($activity->log_type == 'Create Milestone')
                        <div class="activity-icon bg-primary text-white shadow-primary">
                            <i class="mdi mdi-target"></i>
                        </div>
                    @elseif($activity->log_type == 'Create Task')
                        <div class="activity-icon bg-primary text-white shadow-primary">
                            <i class="mdi mdi-format-list-checks"></i>
                        </div>
                    @elseif($activity->log_type == 'Invite User')
                        <div class="activity-icon bg-primary text-white shadow-primary">
                            <i class="mdi mdi-plus"></i>
                        </div>
                    @elseif($activity->log_type == 'Share with Client')
                        <div class="activity-icon bg-primary text-white shadow-primary">
                            <i class="mdi mdi-plus"></i>
                        </div>
                    @elseif($activity->log_type == 'Upload File')
                        <div class="activity-icon bg-primary text-white shadow-primary">
                            <i class="mdi mdi-file"></i>
                        </div>
                    @elseif($activity->log_type == 'Create Timesheet')
                        <div class="activity-icon bg-primary text-white shadow-primary">
                            <i class="mdi mdi-clock"></i>
                        </div>
                    @elseif($activity->log_type == 'Create Bug')
                        <div class="activity-icon bg-primary text-white shadow-primary">
                            <i class="mdi mdi-bug"></i>
                        </div>
                    @elseif($activity->log_type == 'Move Bug')
                        <div class="activity-icon bg-primary text-white shadow-primary">
                            <i class="mdi mdi-cursor-move"></i>
                        </div>
                    @endif
                    <div class="activity-detail">
                        <div class="mb-2">
                            <span class="text-job">{{$activity->created_at->diffForHumans()}}</span>
                        </div>
                        <p>{!! $activity->getRemark() !!}</p>
                    </div>

                </div>
            @endforeach
        </div>
    </div>
</div>
@endif