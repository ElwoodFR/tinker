@extends('layouts.main')
@section('content')
    <section class="section">
        @if($project && $currantWorkspace)

            @auth('client')
                <?php
                $permisions = Auth::user()->getPermission($project->id);
                ?>
            @endauth

            <div class="row mb-2">
                <div class="col-sm-4">
                    <h2 class="section-title" style="margin-top: 10%;"></h2>
                </div>
                <div class="col-sm-8">
                    <div class="text-sm-right">
                        <div class="mt-4 mb-4" >
                            {{-- @auth('client')
                                @if(isset($permisions) && in_array('show gantt',$permisions))
                                    <a href="{{route('client.projects.gantt',[$currantWorkspace->slug,$project->id])}}" class="btn btn-primary">{{ __('Gantt Chart')}}</a>
                                @endif
                            @elseauth
                                <a href="{{route('projects.gantt',[$currantWorkspace->slug,$project->id])}}" class="btn btn-primary">{{ __('Gantt Chart')}}</a>
                            @endauth --}}
                            @auth('client')
                                @if(isset($permisions) && in_array('show task',$permisions))
                                    <a href="{{route('client.projects.task.board',[$currantWorkspace->slug,$project->id])}}" class="btn btn-primary">{{ __('Task Board')}}</a>
                                @endif
                            @elseauth
                                <a href="{{route('projects.task.board',[$currantWorkspace->slug,$project->id])}}" class="btn btn-primary">{{ __('Task Board')}}</a>
                            @endauth
                            {{-- @auth('client')
                                @if(isset($permisions) && in_array('show timesheet',$permisions))
                                    <a href="{{route('client.projects.timesheet',[$currantWorkspace->slug,$project->id])}}" class="btn btn-primary">{{ __('Timesheet')}}</a>
                                @endif
                            @elseauth
                                <a href="{{route('projects.timesheet',[$currantWorkspace->slug,$project->id])}}" class="btn btn-primary">{{ __('Timesheet')}}</a>
                            @endauth --}}
                            @auth('client')
                                @if(isset($permisions) && in_array('show bug report',$permisions))
                                    <a href="{{route('client.projects.bug.report',[$currantWorkspace->slug,$project->id])}}" class="btn btn-primary">{{ __('Bug Report')}}</a>
                                @endif
                            @elseauth
                                <a href="{{route('projects.bug.report',[$currantWorkspace->slug,$project->id])}}" class="btn btn-primary">{{ __('Bug Report')}}</a>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 animated">
                    <!-- project card -->
                    <div class="card author-box card-primary">
                        <div class="card-body">
                            <div class="progress-order-header">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        {{$project->name}}
                                            @if(!$project->is_active)
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                aria-expanded="false">
                                                lock
                                            </a>
                                            @else
                                                @auth('web')
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                    aria-expanded="false">
                                                    <svg viewBox="0 0 24 24" width="16" height="16" stroke="currentColor"
                                                        stroke-width="2" fill="none" stroke-linecap="round"
                                                        stroke-linejoin="round" class="css-i6dzq1" style="
                                                        margin-top: -3px;
                                                        margin-left: 5px;">
                                                        <circle cx="12" cy="12" r="3"></circle>
                                                        <path
                                                            d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z">
                                                        </path>
                                                    </svg>
                                                </a>
                                                <div class="dropdown card-widgets">
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        @if($currantWorkspace->permission == 'Owner')
                                                            <a href="#" class="dropdown-item" data-ajax-popup="true" data-size="lg" data-title="{{ __('Edit Project') }}" data-url="{{route('projects.edit',[$currantWorkspace->slug,$project->id])}}"><i class="mdi mdi-pencil mr-1"></i>{{ __('Edit')}}</a>
                                                            <a href="#" onclick="(confirm('{{__('Are you sure ?')}}')?document.getElementById('delete-form-{{$project->id}}').submit(): '');" class="dropdown-item"><i class="mdi mdi-delete mr-1"></i>{{ __('Delete')}}</a>
                                                            <form id="delete-form-{{$project->id}}" action="{{ route('projects.destroy',[$currantWorkspace->slug,$project->id]) }}" method="POST" style="display: none;">
                                                                @csrf
                                                                @method('DELETE')
                                                            </form>
                                                        @else
                                                            <a href="#" onclick="(confirm('{{__('Are you sure ?')}}')?document.getElementById('leave-form-{{$project->id}}').submit(): '');" class="dropdown-item"><i class="mdi mdi-exit-to-app mr-1"></i>{{ __('Leave')}}</a>
                                                            <form id="leave-form-{{$project->id}}" action="{{ route('projects.leave',[$currantWorkspace->slug,$project->id]) }}" method="POST" style="display: none;">
                                                                @csrf
                                                                @method('DELETE')
                                                            </form>
                                                        @endif
                                                    </div>
                                                </div>
                                                @endauth
                                            @endif
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        @if($project->status == 'Finished')
                                            <div class="badge badge-success">{{ __('Finished')}}</div>
                                        @elseif($project->status == 'Ongoing')
                                            <div class="badge badge-secondary">{{ __('Ongoing')}}</div>
                                        @else
                                            <div class="badge badge-warning">{{ __('OnHold')}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="mt-4 font-weight-bold">{{ __('Project Overview') }}:</div>

                            <div class="author-box-description">
                                {{$project->description}}
                            </div>

                            <div class="row mt-3">
                                @if($project->start_date)
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <div class="font-weight-bold">{{ __('Start Date')}}</div>
                                            <p>{{Utility::dateFormat($project->start_date)}}</p>
                                        </div>
                                    </div>
                                @endif
                                @if($project->end_date)
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <div class="font-weight-bold">{{ __('End Date')}}</div>
                                            <p>{{Utility::dateFormat($project->end_date)}}</p>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-4">
                                    <div class="mb-4">
                                        <div class="font-weight-bold">{{ __('Budget')}}</div>
                                        <p>${{ number_format($project->budget) }}</p>
                                    </div>
                                </div>
                            </div>

                        </div> <!-- end card-body-->

                    </div> <!-- end card-->

                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-one">

                                    <div class="widget-heading">
                                    </div>

                                    <div class="widget-content">
                                        <div class="invoice-box">
                                                <h5>{{__('Days left')}}</h5>
                                                <p class="acc-amount">
                                                @php
                                                    $datetime1 = new DateTime($project->end_date);
                                                    $datetime2 = new DateTime(date('Y-m-d'));
                                                    $interval = $datetime1->diff($datetime2);
                                                    $days = $interval->format('%a')
                                                @endphp
                                                {{$days}}
                                                </p>
                                        </div>
                                    </div>

                                </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-one">

                                    <div class="widget-heading">
                                    </div>

                                    <div class="widget-content">
                                        <div class="invoice-box">
                                                <h5>{{__('Total task')}}</h5>
                                                <p class="acc-amount">
                                                    {{$project->countTask()}}
                                                </p>
                                        </div>
                                    </div>

                                </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-one">

                                    <div class="widget-heading">
                                    </div>

                                    <div class="widget-content">
                                        <div class="invoice-box">
                                                <h5>{{__('Total bug')}}</h5>
                                                <p class="acc-amount">
                                                    {{$project->countBugReport()}}
                                                </p>
                                        </div>
                                    </div>

                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-6 col-lg-12 col-md-6 col-sm-12 col-12 layout-spacing">
                            <div class="widget widget-table-one">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        {{__('Team Members')}} ({{count($project->users)}})
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        @if($currantWorkspace->permission == 'Owner')
                                            <a href="#" class="btn btn-sm btn-primary" data-ajax-popup="true" data-title="{{ __('Invite') }}" data-url="{{route('projects.invite.popup',[$currantWorkspace->slug,$project->id])}}">{{ __('Invite')}}</a>
                                        @endif
                                    </div>
                                </div>
                        
                                <div class="widget-content" style="margin-top: 15px;">
                                    @foreach($project->users as $user)
                                    <div class="transactions-list">
                                        <div class="t-item">
                                            <div class="t-company-name">
                                                <div class="t-icon">
                                                        <img class="mr-3 rounded-circle" width="50" @if($user->avatar) src="{{asset('/storage/avatars/'.$user->avatar)}}" @else avatar="{{ $user->name }}" @endif alt="avatar">
                                                </div>
                                                <div class="t-name">
                                                    <h4>{{$user->name}}</h4>
                                                    <p class="meta-date">{{$user->email}}</p>
                                                </div>
                        
                                            </div>
                                            <div class="t-rate rate-dec">
                                                @auth('web')
                                                @if($currantWorkspace->permission == 'Owner' && $user->id != Auth::user()->id)
                                                    <a href="#" class="float-right" onclick="(confirm('{{__('Are you sure ?')}}')?document.getElementById('delete-user-{{$user->id}}').submit(): '');">
                                                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="#e7515a" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                    </a>
                                                    <form id="delete-user-{{$user->id}}" action="{{ route('projects.user.delete',[$currantWorkspace->slug,$project->id,$user->id]) }}" method="POST" style="display: none;">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                @endif
                                            @endauth
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                        
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-12 col-md-6 col-sm-12 col-12 layout-spacing">
                            <div class="widget widget-table-one">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        {{__('Clients')}} ({{count($project->clients)}})
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        @if($currantWorkspace->permission == 'Owner')
                                                <a href="#" class="btn btn-sm btn-primary" data-ajax-popup="true" data-title="{{ __('Share to Clients') }}" data-url="{{route('projects.share.popup',[$currantWorkspace->slug,$project->id])}}">{{ __('Share')}}</a>
                                        @endif
                                    </div>
                                </div>
                        
                                <div class="widget-content" style="margin-top: 15px;">
                                    @foreach($project->clients as $client)
                                    <div class="transactions-list">
                                        <div class="t-item">
                                            <div class="t-company-name">
                                                <div class="t-icon">
                                                    <img class="mr-3 rounded-circle" width="50" @if($client->avatar) src="{{asset('/storage/avatars/'.$client->avatar)}}" @else avatar="{{ $client->name }}" @endif alt="avatar">
                                                </div>
                                                <div class="t-name">
                                                    <h4>{{$client->name}}</h4>
                                                    <p class="meta-date">{{$client->email}}</p>
                                                </div>
                        
                                            </div>
                                            <div class="t-rate rate-dec">
                                                @auth('web')
                                                    @if($currantWorkspace->permission == 'Owner')
                                                        <a href="#" class="float-right" onclick="(confirm('{{__('Are you sure ?')}}')?document.getElementById('delete-client-{{$client->id}}').submit(): '');">
                                                            <svg viewBox="0 0 24 24" width="24" height="24" stroke="#e7515a" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                        </a>
                                                        <a href="#" class="float-right mr-1" data-ajax-popup="true" data-size="lg" data-title="{{__('Edit Permission')}}" data-url="{{route('projects.client.permission',[$currantWorkspace->slug,$project->id,$client->id])}}">
                                                            <svg viewBox="0 0 24 24" width="24" height="24" stroke="#1b55e2" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>
                                                        </a>
                                                        <form id="delete-client-{{$client->id}}" action="{{ route('projects.client.delete',[$currantWorkspace->slug,$project->id,$client->id]) }}" method="POST" style="display: none;">
                                                            @csrf
                                                            @method('DELETE')
                                                        </form>
                                                    @endif
                                                @endauth
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    @if((isset($permisions) && in_array('show milestone',$permisions)) || $currantWorkspace->permission == 'Owner')
                        <div class="layout-spacing">
                            <div class="widget widget-table-one">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        {{__('Milestones')}} ({{count($project->milestones)}})
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        @if((isset($permisions) && in_array('create milestone',$permisions)) || $currantWorkspace->permission == 'Owner')
                                            <a href="#" class="btn btn-sm btn-primary" data-ajax-popup="true" data-title="{{ __('Create Milestone') }}" data-url="@auth('web'){{route('projects.milestone',[$currantWorkspace->slug,$project->id])}}@elseauth{{route('client.projects.milestone',[$currantWorkspace->slug,$project->id])}}@endauth">{{__('Create Milestone')}}</a>
                                        @endif
                                    </div>
                                </div>
                        
                                <div class="widget-content" style="margin-top: 15px;">
                                    @foreach($project->milestones as $key => $milestone)
                                    <div class="transactions-list">
                                        <div class="t-item">
                                            <div class="t-company-name">
                                                <div class="t-icon">
                                                    <div class="icon">
                                                        #{{$key+1}}
                                                    </div>
                                                </div>
                                                <div class="t-name">
                                                    <a href="#" class="milestone-detail" data-ajax-popup="true" data-title="{{ __('Milestones Details') }}" data-url="@auth('web'){{route('projects.milestone.show',[$currantWorkspace->slug,$milestone->id])}}@elseauth{{route('client.projects.milestone.show',[$currantWorkspace->slug,$milestone->id])}}@endauth">{{$milestone->title}}</a>
                                                    <p class="meta-date">
                                                        @if($milestone->status == 'incomplete')
                                                            <label class="badge badge-warning">{{__('Incomplete')}}</label>
                                                        @endif
                                                        @if($milestone->status == 'complete')
                                                            <label class="badge badge-success">{{__('Complete')}}</label>
                                                        @endif
                                                    </p>
                                                </div>
                        
                                            </div>
                                            <div class="t-rate rate-dec">
                                                <p>{{__('Milestone Cost')}}: <span>${{number_format($milestone->cost)}}</span></p>
                                            </div>
                                            @if($currantWorkspace->permission == 'Owner')
                                            <div class="float-right" style="margin-top: 12px;">
                                                    <a href="#" data-ajax-popup="true" data-title="{{ __('Edit Milestone') }}" data-url="{{route('projects.milestone.edit',[$currantWorkspace->slug,$milestone->id])}}">
                                                        <svg viewBox="0 0 24 24" width="20" height="20" stroke="#1b55e2" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                                    </a>
                                                    <a href="#" onclick="(confirm('{{__('Are you sure ?')}}')?document.getElementById('delete-form1-{{$milestone->id}}').submit(): '');">
                                                        <svg viewBox="0 0 24 24" width="20" height="20" stroke="#e7515a" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                    </a>
                                                    <form id="delete-form1-{{$milestone->id}}" action="{{ route('projects.milestone.destroy',[$currantWorkspace->slug,$milestone->id]) }}" method="POST" style="display: none;">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                            </div>
                                        @elseif(isset($permisions))
                                        <div class="float-right">
                                                @if(in_array('edit milestone',$permisions))
                                                    <a href="#" class="btn btn-sm btn-outline-primary" data-ajax-popup="true" data-title="{{ __('Edit Milestone') }}" data-url="{{route('client.projects.milestone.edit',[$currantWorkspace->slug,$milestone->id])}}"><i class="mdi mdi-pencil"></i></a>
                                                @endif
                                                @if(in_array('delete milestone',$permisions))
                                                    <a href="#" class="btn btn-sm btn-outline-danger" onclick="(confirm('{{__('Are you sure ?')}}')?document.getElementById('delete-form1-{{$milestone->id}}').submit(): '');"><i class="mdi mdi-delete"></i></a>
                                                    <form id="delete-form1-{{$milestone->id}}" action="{{ route('client.projects.milestone.destroy',[$currantWorkspace->slug,$milestone->id]) }}" method="POST" style="display: none;">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                @endif
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    <!-- @if((isset($permisions) && in_array('show uploading',$permisions)) || $currantWorkspace->permission == 'Owner')
                        <div class="card author-box card-primary">
                            <div class="card-body">
                                <div class="author-box-name mb-4">
                                    {{__('Files')}}
                                </div>
                                <div class="col-md-12 dropzone" id="dropzonewidget">
                                    <div class="dz-message" data-dz-message><span>{{__('Drop files here to upload')}}</span></div>
                                </div>
                            </div>
                        </div>
                    @endif -->
                    <!-- end card-->
                </div> <!-- end col -->

                <div class="col-md-4 animated">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>{{ __('Progress') }}</h4>
                        </div>
                        <div class="card-body">
                            <div style="height: 283px;">
                                <div id="revenueMonthly"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end card-->
                    @if((isset($permisions) && in_array('show activity',$permisions)) || $currantWorkspace->permission == 'Owner')
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>{{ __('Activity') }}</h4>
                        </div>
                        <div class="card-body pr-2 pl-3">
                            <div class="activities top-10-scroll">
                                @foreach($project->activities as $activity)
                                    <div class="activity">
                                        @if($activity->log_type == 'Move')
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-cursor-move"></i>
                                            </div>
                                        @elseif($activity->log_type == 'Create Milestone')
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-target"></i>
                                            </div>
                                        @elseif($activity->log_type == 'Create Task')
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-format-list-checks"></i>
                                            </div>
                                        @elseif($activity->log_type == 'Invite User')
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-plus"></i>
                                            </div>
                                        @elseif($activity->log_type == 'Share with Client')
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-plus"></i>
                                            </div>
                                        @elseif($activity->log_type == 'Upload File')
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-file"></i>
                                            </div>
                                        @elseif($activity->log_type == 'Create Timesheet')
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-clock"></i>
                                            </div>
                                        @elseif($activity->log_type == 'Create Bug')
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-bug"></i>
                                            </div>
                                        @elseif($activity->log_type == 'Move Bug')
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-cursor-move"></i>
                                            </div>
                                        @endif
                                        <div class="activity-detail">
                                            <div class="mb-2">
                                                <span class="text-job">{{$activity->created_at->diffForHumans()}}</span>
                                            </div>
                                            <p>{!! $activity->getRemark() !!}</p>
                                        </div>

                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        @else
            <div class="container mt-5">
                <div class="page-error">
                    <div class="page-inner">
                        <h1>404</h1>
                        <div class="page-description">
                            {{ __('Page Not Found') }}
                        </div>
                        <div class="page-search">
                            <p class="text-muted mt-3">{{ __('It\'s looking like you may have taken a wrong turn. Don\'t worry... it happens to the best of us. Here\'s a little tip that might help you get back on track.')}}</p>
                            <div class="mt-3">
                                <a class="btn btn-info mt-3" href="{{route('home')}}"><i class="mdi mdi-reply"></i> {{ __('Return Home')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif


    </section>
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/css/dropzone.min.css')}}">
    <link rel="stylesheet" href="{{asset('src/css/dashboard/dash_1.css')}}">
@endpush
@push('scripts')
    <!-- third party js -->
    <script src="{{ asset('plugins/apex/apexcharts.min.js') }}"></script>
    <script>

var options1 = {
  chart: {
    fontFamily: 'Nunito, sans-serif',
    height: 270,
    type: 'area',
    zoom: {
        enabled: false
    },
    dropShadow: {
      enabled: true,
      opacity: 0.3,
      blur: 5,
      left: -7,
      top: 22
    },
    toolbar: {
      show: false
    },
    events: {
      mounted: function(ctx, config) {
        const highest1 = ctx.getHighestValueInSeries(0);
        const highest2 = ctx.getHighestValueInSeries(1);

        ctx.addPointAnnotation({
          x: new Date(ctx.w.globals.seriesX[0][ctx.w.globals.series[0].indexOf(highest1)]).getTime(),
          y: highest1,
          label: {
            style: {
              cssClass: 'd-none'
            }
          },
          customSVG: {
              SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#1b55e2" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
              cssClass: undefined,
              offsetX: -8,
              offsetY: 5
          }
        })

        ctx.addPointAnnotation({
          x: new Date(ctx.w.globals.seriesX[1][ctx.w.globals.series[1].indexOf(highest2)]).getTime(),
          y: highest2,
          label: {
            style: {
              cssClass: 'd-none'
            }
          },
          customSVG: {
              SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#e7515a" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
              cssClass: undefined,
              offsetX: -8,
              offsetY: 5
          }
        })
      },
    }
  },
  colors: ['#1b55e2', '#e7515a', '#0acf97', '#727cf5', '#875767'],
  dataLabels: {
      enabled: false
  },
  markers: {
    discrete: [{
    seriesIndex: 0,
    dataPointIndex: 7,
    fillColor: '#000',
    strokeColor: '#000',
    size: 5
  }, {
    seriesIndex: 2,
    dataPointIndex: 11,
    fillColor: '#000',
    strokeColor: '#000',
    size: 4
  }]
  },
  subtitle: {
    // text: 'Task Overview',
    align: 'left',
    margin: 0,
    offsetX: -10,
    offsetY: 35,
    floating: false,
    style: {
      fontSize: '14px',
      color:  '#888ea8'
    }
  },
  title: {
    align: 'left',
    margin: 0,
    offsetX: -10,
    offsetY: 0,
    floating: false,
    style: {
      fontSize: '25px',
      color:  '#bfc9d4'
    },
  },
  stroke: {
      show: true,
      curve: 'smooth',
      width: 2,
      lineCap: 'square'
  },
  series: [{
      name: "{{ __('Todo') }}",
      data: {!! json_encode($chartData['todo']) !!}
  }, {
      name: "{{ __('In Progress') }}",
      data: {!! json_encode($chartData['progress']) !!}
  }, {
      name: "{{ __('Review') }}",
      data: {!! json_encode($chartData['review']) !!}
  }, {
      name: "{{ __('Done') }}",
      data: {!! json_encode($chartData['done']) !!}
  }],
  labels: {!! json_encode($chartData['label']) !!},
  xaxis: {
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    },
    crosshairs: {
      show: true
    },
    labels: {
      offsetX: 0,
      offsetY: 5,
      style: {
          fontSize: '12px',
          fontFamily: 'Nunito, sans-serif',
          cssClass: 'apexcharts-xaxis-title',
      },
    }
  },
  yaxis: {
    labels: {
      formatter: function(value, index) {
        return (value)
      },
      offsetX: -22,
      offsetY: 0,
      style: {
          fontSize: '12px',
          fontFamily: 'Nunito, sans-serif',
          cssClass: 'apexcharts-yaxis-title',
      },
    }
  },
  grid: {
    borderColor: '#191e3a',
    strokeDashArray: 5,
    xaxis: {
        lines: {
            show: true
        }
    },   
    yaxis: {
        lines: {
            show: false,
        }
    },
    padding: {
      top: 0,
      right: 0,
      bottom: 0,
      left: -10
    }, 
  }, 
  legend: {
    position: 'top',
    horizontalAlign: 'right',
    offsetY: -50,
    fontSize: '16px',
    fontFamily: 'Nunito, sans-serif',
    markers: {
      width: 10,
      height: 10,
      strokeWidth: 0,
      strokeColor: '#fff',
      fillColors: undefined,
      radius: 12,
      onClick: undefined,
      offsetX: 0,
      offsetY: 0
    },    
    itemMargin: {
      horizontal: 0,
      vertical: 20
    }
  },
  tooltip: {
    theme: 'dark',
    marker: {
      show: true,
    },
    x: {
      show: false,
    }
  },
  fill: {
      type:"gradient",
      gradient: {
          type: "vertical",
          shadeIntensity: 1,
          inverseColors: !1,
          opacityFrom: .28,
          opacityTo: .05,
          stops: [45, 100]
      }
  },
  responsive: [{
    breakpoint: 575,
    options: {
      legend: {
          offsetY: -30,
      },
    },
  }]
}

var chart1 = new ApexCharts(
    document.querySelector("#revenueMonthly"),
    options1
);

chart1.render();


    </script>
    <!-- third party js ends -->

    <script src="{{asset('assets/js/dropzone.min.js')}}"></script>
    <script>
        Dropzone.autoDiscover = false;
        myDropzone = new Dropzone("#dropzonewidget", {
            maxFiles: 20,
            maxFilesize: 2,
            parallelUploads: 1,
            acceptedFiles: ".jpeg,.jpg,.png,.pdf,.doc,.txt",
            url: "{{route('projects.file.upload',[$currantWorkspace->slug,$project->id])}}",
            success: function (file, response) {
                if (response.is_success) {
                    dropzoneBtn(file, response);
                } else {
                    myDropzone.removeFile(file);
                    toastr('{{__('Error')}}', response.error, 'error');
                }
            },
            error: function (file, response) {
                myDropzone.removeFile(file);
                if (response.error) {
                    toastr('{{__('Error')}}', response.error, 'error');
                } else {
                    toastr('{{__('Error')}}', response, 'error');
                }
            }
        });
        myDropzone.on("sending", function (file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
            formData.append("project_id", {{$project->id}});
        });

        @if(isset($permisions) && in_array('show uploading',$permisions))
            $(".dz-hidden-input").prop("disabled",true);
            myDropzone.removeEventListeners();
        @endif

        function dropzoneBtn(file, response) {

            var html = document.createElement('div');

            var download = document.createElement('a');
            download.setAttribute('href', response.download);
            download.setAttribute('class', "btn btn btn-outline-primary btn-sm mt-1 mr-1");
            download.setAttribute('data-toggle', "tooltip");
            download.setAttribute('data-original-title', "{{__('Download')}}");
            download.innerHTML = "<i class='mdi mdi-download'></i>";
            html.appendChild(download);

            @if(isset($permisions) && in_array('show uploading',$permisions))
            @else
                var del = document.createElement('a');
                del.setAttribute('href', response.delete);
                del.setAttribute('class', "btn btn-outline-danger btn-sm mt-1");
                del.setAttribute('data-toggle', "tooltip");
                del.setAttribute('data-original-title', "{{__('Delete')}}");
                del.innerHTML = "<i class='mdi mdi-delete'></i>";

                del.addEventListener("click", function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (confirm("Are you sure ?")) {
                        var btn = $(this);
                        $.ajax({
                            url: btn.attr('href'),
                            data: {_token: $('meta[name="csrf-token"]').attr('content')},
                            type: 'DELETE',
                            success: function (response) {
                                if (response.is_success) {
                                    btn.closest('.dz-image-preview').remove();
                                } else {
                                    toastr('{{__('Error')}}', response.error, 'error');
                                }
                            },
                            error: function (response) {
                                response = response.responseJSON;
                                if (response.is_success) {
                                    toastr('{{__('Error')}}', response.error, 'error');
                                } else {
                                    toastr('{{__('Error')}}', response, 'error');
                                }
                            }
                        })
                    }
                });
                html.appendChild(del);
            @endif





            file.previewTemplate.appendChild(html);
        }

        @php
            $files = $project->files;
        @endphp
        @foreach($files as $file)
        // Create the mock file:
        var mockFile = {name: "{{$file->file_name}}", size: {{filesize(storage_path('project_files/'.$file->file_path))}} };
        // Call the default addedfile event handler
        myDropzone.emit("addedfile", mockFile);
        // And optionally show the thumbnail of the file:
        myDropzone.emit("thumbnail", mockFile, "{{asset('storage/project_files/'.$file->file_path)}}");
        myDropzone.emit("complete", mockFile);

        dropzoneBtn(mockFile, {download: "@auth('web'){{route('projects.file.download',[$currantWorkspace->slug,$project->id,$file->id])}}@elseauth{{route('client.projects.file.download',[$currantWorkspace->slug,$project->id,$file->id])}}@endauth", delete: "{{route('projects.file.delete',[$currantWorkspace->slug,$project->id,$file->id])}}"});

        @endforeach
    </script>
@endpush
