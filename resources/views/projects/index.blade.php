@extends('layouts.main')

@section('content')
<div class="row app-notes layout-top-spacing" id="cancel-row">
    <div class="col-lg-12">
        @if($projects && $currantWorkspace)
        <div class="row mb-2">
            <div class="col-sm-4">
                @auth('web')
                @if($currantWorkspace->creater->id == Auth::user()->id)
                <button type="button" class="btn btn-primary" data-ajax-popup="true" data-size="lg"
                    data-title="{{ __('Create New Project') }}"
                    data-url="{{route('projects.create',$currantWorkspace->slug)}}">
                    <i class="mdi mdi-plus"></i> {{ __('Create Project') }}
                </button>
                @endif
                @endauth
            </div>
            <div class="col-sm-8">
                <div class="text-sm-right status-filter">
                    <div class="btn-group mb-3">
                        <button type="button" class="btn btn-primary" data-status="All">{{ __('All')}}</button>
                    </div>
                    <div class="btn-group mb-3 ml-1">
                        <button type="button" class="btn btn-light" data-status="Ongoing">{{ __('Ongoing')}}</button>
                        <button type="button" class="btn btn-light" data-status="Finished">{{ __('Finished')}}</button>
                        <button type="button" class="btn btn-light" data-status="OnHold">{{ __('OnHold')}}</button>
                    </div>

                </div>
            </div><!-- end col-->
        </div>
        <div class="row">
            @foreach ($projects as $project)
            <div class="col-sm-4">
                <div class="card animated filter {{$project->status}}">
                    <div class="card-body">

                        <div class="progress-order">
                            <div class="progress-order-header">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        @if($project->is_active)
                                        <a href="@auth('web'){{route('projects.show',[$currantWorkspace->slug,$project->id])}}@elseauth{{route('client.projects.show',[$currantWorkspace->slug,$project->id])}}@endauth"
                                            title="{{ $project->name }}" class="text-title">{{ $project->name }}</a>
                                        @else
                                        <a href="#" title="{{ __('Locked') }}"
                                            class="text-title">{{ $project->name }}</a>
                                        @endif
                                        @if(!$project->is_active)
                                        <a href="#" class="btn" title="{{__('Locked')}}">
                                            lock
                                        </a>
                                        @else
                                        @auth('web')
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">
                                            <svg viewBox="0 0 24 24" width="16" height="16" stroke="currentColor"
                                                stroke-width="2" fill="none" stroke-linecap="round"
                                                stroke-linejoin="round" class="css-i6dzq1" style="
                                                margin-top: -3px;
                                                margin-left: 5px;
                                            ">
                                                <circle cx="12" cy="12" r="3"></circle>
                                                <path
                                                    d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z">
                                                </path>
                                            </svg>
                                        </a>
                                        <div class="dropdown card-widgets">
                                            <div class="dropdown-menu dropdown-menu-right">

                                                @if($currantWorkspace->permission == 'Owner')
                                                <a href="#" class="dropdown-item" data-ajax-popup="true" data-size="lg"
                                                    data-title="{{ __('Edit Project') }}"
                                                    data-url="{{route('projects.edit',[$currantWorkspace->slug,$project->id])}}"><i
                                                        class="mdi mdi-pencil mr-1"></i>{{ __('Edit')}}</a>
                                                <a href="#"
                                                    onclick="(confirm('{{__('Are you sure ?')}}')?document.getElementById('delete-form-{{$project->id}}').submit(): '');"
                                                    class="dropdown-item"><i
                                                        class="mdi mdi-delete mr-1"></i>{{ __('Delete')}}</a>
                                                <form id="delete-form-{{$project->id}}"
                                                    action="{{ route('projects.destroy',[$currantWorkspace->slug,$project->id]) }}"
                                                    method="POST" style="display: none;">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                <a href="#" class="dropdown-item" class="dropdown-item"
                                                    data-ajax-popup="true" data-size="lg"
                                                    data-title="{{ __('Invite Users') }}"
                                                    data-url="{{route('projects.invite.popup',[$currantWorkspace->slug,$project->id])}}"><i
                                                        class="mdi mdi-email-outline mr-1"></i>{{ __('Invite')}}</a>
                                                <a href="#" class="dropdown-item" data-ajax-popup="true" data-size="lg"
                                                    data-title="{{ __('Share to Clients') }}"
                                                    data-url="{{route('projects.share.popup',[$currantWorkspace->slug,$project->id])}}"><i
                                                        class="mdi mdi-email-outline mr-1"></i>{{ __('Share')}}</a>
                                                @else
                                                <a href="#"
                                                    onclick="(confirm('{{__('Are you sure ?')}}')?document.getElementById('leave-form-{{$project->id}}').submit(): '');"
                                                    class="dropdown-item"><i
                                                        class="mdi mdi-exit-to-app mr-1"></i>{{ __('Leave')}}</a>
                                                <form id="leave-form-{{$project->id}}"
                                                    action="{{ route('projects.leave',[$currantWorkspace->slug,$project->id]) }}"
                                                    method="POST" style="display: none;">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                @endif

                                            </div>
                                            @endauth
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        @if($project->status == 'Finished')
                                        <span class="shadow-none badge badge-success">{{ __('Finished')}}</span>
                                        @elseif($project->status == 'Ongoing')
                                        <span class="shadow-none badge badge-info">{{ __('Ongoing')}}</span>
                                        @else
                                        <span class="shadow-none badge badge-warning">{{ __('OnHold')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="progress-order-body">
                                <div class="row mt-4">
                                    <div class="col-md-12 text-left">
                                        <div class="author-box-description">
                                            <p>
                                                {{Str::limit($project->description, $limit = 50, $end = '...')}}
                                            </p>
                                        </div>
                                        <p class="mb-1">
                                            <span class="pr-2 text-nowrap mb-2 d-inline-block">
                                                <i class="mdi mdi-format-list-bulleted-type text-muted"></i>
                                                <b>{{$project->countTask()}}</b> {{ __('Tasks')}}
                                            </span>
                                            <span class="text-nowrap mb-2 d-inline-block">
                                                <i class="mdi mdi-comment-multiple-outline text-muted"></i>
                                                <b>{{$project->countTaskComments()}}</b> {{ __('Comments')}}
                                            </span>
                                        </p>
                                    </div>
                                    <div class="col-md-12">
                                        <ul class="list-inline badge-collapsed-img mb-0 mb-3">
                                            @foreach($project->users as $user)
                                            @if($user->pivot->is_active)
                                            <li class="list-inline-item chat-online-usr">
                                                <img @if($user->avatar)
                                                src="{{asset('/storage/avatars/'.$user->avatar)}}" @else
                                                avatar="{{ $user->name }}" @endif class="rounded-circle">
                                            </li>
                                            @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            @endforeach
        </div>
        @else
        <div class="container mt-5">
            <div class="page-error">
                <div class="page-inner">
                    <h1>404</h1>
                    <div class="page-description">
                        {{ __('Page Not Found') }}
                    </div>
                    <div class="page-search">
                        <p class="text-muted mt-3">
                            {{ __('It\'s looking like you may have taken a wrong turn. Don\'t worry... it happens to the best of us. Here\'s a little tip that might help you get back on track.')}}
                        </p>
                        <div class="mt-3">
                            <a class="btn btn-info mt-3" href="{{route('home')}}"><i class="mdi mdi-reply"></i>
                                {{ __('Return Home')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endif
    </div>
</div>
@endsection

@push('style')
{{--    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">--}}
<link href="{{ asset('assets/css/vendor/bootstrap-tagsinput.css') }}" rel="stylesheet">
@endpush
