<header class="header navbar navbar-expand-sm">

    <ul class="navbar-item theme-brand flex-row  text-center">
        <li class="nav-item theme-logo">
            <a href="/">
                <img src="{{asset('storage/logo/pni.png') }}" class="navbar-logo" alt="logo">
            </a>
        </li>
    </ul>
  
    <ul class="navbar-item flex-row ml-md-0 ml-auto">
        <li class="nav-item align-self-center search-animated">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                class="feather feather-search toggle-search">
                <circle cx="11" cy="11" r="8"></circle>
                <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
            </svg>
            <form class="form-inline search-full form-inline search" role="search">
                <div class="search-bar">
                    <input type="text" class="form-control search-form-control  ml-lg-auto" placeholder="Search...">
                </div>
            </form>
        </li>
    </ul>
  
    <ul class="navbar-item flex-row ml-md-auto">
  
        @if(isset($currantWorkspace) && $currantWorkspace && $currantWorkspace->permission == 'Owner')
        @php
          $currantLang = basename(App::getLocale());
        @endphp
        <li class="nav-item dropdown language-dropdown">
            <a href="javascript:void(0);" class="nav-link dropdown-toggle" id="language-dropdown" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                @if($currantLang == 'en')
                    <img src="{{asset('storage/lang/en.png') }}" class="flag-width" alt="flag">
                @else
                    <img src="{{asset('storage/lang/fr.png') }}" class="flag-width" alt="flag">
                @endif
            </a>
            <div class="dropdown-menu position-absolute" aria-labelledby="language-dropdown">
                @foreach($currantWorkspace->languages() as $lang)
                @if($currantLang != $lang)
                <!-- item-->
                <a href="{{route('change_lang_workspace',[$currantWorkspace->id,$lang])}}" class="dropdown-item">
                    @if($currantLang != 'en')
                        <img src="{{asset('storage/lang/en.png') }}" class="flag-width" alt="flag">
                        <span class="align-middle">{{Str::upper($lang)}}</span>
                    @else
                        <img src="{{asset('storage/lang/fr.png') }}" class="flag-width" alt="flag">
                        <span class="align-middle">{{Str::upper($lang)}}</span>
                    @endif
                </a>
                @endif
                @endforeach
            </div>
        </li>
        @endif

        @if(isset($currantWorkspace) && $currantWorkspace)
        @auth('web')
        <li class="nav-item dropdown message-dropdown">
            <a href="javascript:void(0);" class="nav-link dropdown-toggle" id="messageDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
            </a>
            <div class="dropdown-menu p-0 position-absolute" aria-labelledby="messageDropdown">
                <div class="">

                    <a class="dropdown-item">
                        <div class="">
    
                            <div class="media">
                                <div class="dropdown-menu dropdown-list dropdown-menu-right">
                                    <div class="dropdown-header">{{__('Messages')}}
                                        <div class="float-right" style="margin-bottom: 10px;">
                                            <a href="#" class="mark_all_as_read_message">{{__('Mark All As Read')}}</a>
                                        </div>
                                    </div>
                                    <div class="dropdown-list-content dropdown-list-message" tabindex="3">
                        
                                    </div>
                                    <div class="dropdown-footer text-center">
                                        <a href="{{route('chats.index',$currantWorkspace->slug)}}">{{__('View All')}} <i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </a>
                </div>
            </div>
        </li>
    
        <li class="nav-item dropdown notification-dropdown">
            @php
                $notifications = Auth::user()->notifications($currantWorkspace->id)
            @endphp
            <a href="javascript:void(0);" class="nav-link dropdown-toggle" id="notificationDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg>
                <span class="@if(count($notifications)) badge badge-success beep @endif""></span>
            </a>
            <div class="dropdown-menu position-absolute" aria-labelledby="notificationDropdown">
                <div class="float-right" style="margin-bottom: 10px;">
                    <a href="#" class="mark_all_as_read">{{__('Mark All As Read')}}</a>
                </div>
                <div class="notification-scroll">
                    @foreach($notifications as $notification)
                            <div class="media-body">
                                <div class="notification-para">
                                    {!! $notification->toHtml() !!}
                                </div>
                            </div>
                    @endforeach    
                </div>
            </div>
        </li>
    
        @endauth
        @endif
  
        <li class="nav-item dropdown user-profile-dropdown">
            <a href="javascript:void(0);" class="nav-link dropdown-toggle user-dropdown" id="userProfileDropdown"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <img @if(Auth::user()->avatar) src="{{asset('/storage/avatars/'.Auth::user()->avatar)}}" @else
                avatar="{{ Auth::user()->name }}" @endif alt="user-image" class="mr-1"></a>
            </a>
            <div class="dropdown-menu position-absolute" aria-labelledby="userProfileDropdown">
                <div class="">
                    <div class="dropdown-item">
                        @foreach(Auth::user()->workspace as $workspace)
                        <a href="@if($currantWorkspace->id == $workspace->id)#@else{{ route('change_workspace',$workspace->id) }}@endif"
                            title="{{ $workspace->name }}" class="dropdown-item notify-item">
                            @if($currantWorkspace->id == $workspace->id)
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>
                            @endif
                            {{ $workspace->name }}
                              @if(isset($workspace->pivot->permission))
                                @if($workspace->pivot->permission =='Owner')
                                <span class="badge badge-primary">{{$workspace->pivot->permission}}</span>
                                @else
                              <span class="badge badge-secondary">{{__('Shared')}}</span>
                              @endif
                            @endif
                        </a>
                        @endforeach
                    </div>
                    @if(isset($currantWorkspace) && $currantWorkspace)
                    <div class="dropdown-divider"></div>
                    @endif
                    <div class="dropdown-item">
                        <a href="#" class="dropdown-item notify-item" data-toggle="modal"
                            data-target="#modelCreateWorkspace">
                            <i class="mdi mdi-plus"></i>
                            <span>{{ __('Create New Workspace')}}</span>
                        </a>
                    </div>
                    <div class="dropdown-item">
                        @if(isset($currantWorkspace) && $currantWorkspace)
                          @if(Auth::user()->id == $currantWorkspace->created_by)
                          <a href="#" class="dropdown-item notify-item"
                              onclick="(confirm('Are you sure ?')?document.getElementById('remove-workspace-form').submit(): '');">
                              <i class=" mdi mdi-delete-outline"></i>
                              <span>{{ __('Remove Me From This Workspace')}}</span>
                          </a>
                          <form id="remove-workspace-form"
                              action="{{ route('delete_workspace', ['id' => $currantWorkspace->id]) }}" method="POST"
                              style="display: none;">
                              @csrf
                              @method('DELETE')
                          </form>
                          @else
                          <a href="#" class="dropdown-item notify-item"
                              onclick="(confirm('Are you sure ?')?document.getElementById('remove-workspace-form').submit(): '');">
                              <i class=" mdi mdi-delete-outline"></i>
                              <span>{{ __('Leave Me From This Workspace')}}</span>
                          </a>
                          <form id="remove-workspace-form"
                              action="{{ route('leave_workspace', ['id' => $currantWorkspace->id]) }}" method="POST"
                              style="display: none;">
                              @csrf
                              @method('DELETE')
                          </form>
                          @endif
                        @endif
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="dropdown-item">
                        <a href="{{ route('users.my.account') }}" class="dropdown-item has-icon">
                            <i class="mdi mdi-account-circle mr-1"></i> {{ __('My Account')}}
                        </a>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="dropdown-item">
                        <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger"
                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="mdi mdi-logout mr-1"></i> {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
  
                </div>
            </div>
        </li>
  
    </ul>
  </header>