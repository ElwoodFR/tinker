@extends('layouts.main')

@section('content')
<div class="app-notes layout-top-spacing" id="cancel-row">
    @if($currantWorkspace)
            <h2 class="section-title"></h2>
            <div class="row">
                <div class="col-12">
                    @if(empty(env('PUSHER_APP_ID')) || empty(env('PUSHER_APP_KEY')) || empty(env('PUSHER_APP_SECRET')) || empty(env('PUSHER_APP_CLUSTER')))
                        <div class="alert alert-warning"><i class="dripicons-warning"></i> {{ __('Please Add Pusher Detail in .env') }}</div>
                    @endif
                   <!-- 
                    @if(empty(env('MAIL_DRIVER')) || empty(env('MAIL_HOST')) || empty(env('MAIL_PORT')) || empty(env('MAIL_USERNAME'))  || empty(env('MAIL_PASSWORD'))  || empty(env('MAIL_PASSWORD')))
                        <div class="alert alert-warning"><i class="dripicons-warning"></i> {{ __('Please Add Mail Setting Detail in .env') }}</div>
                    @endif
                     --> 
                </div>
              </div>
              <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                            <div class="widget widget-account-invoice-one">

                                <div class="widget-heading">
                                </div>

                                <div class="widget-content">
                                    <div class="invoice-box">
                                            <h5>{{ __('Total Projects') }}</h5>
                                            <p class="acc-amount">{{$totalProject}}</p>
                                    </div>
                                </div>

                            </div>
                    </div> 
                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                            <div class="widget widget-account-invoice-one">

                                <div class="widget-heading">
                                </div>

                                <div class="widget-content">
                                    <div class="invoice-box">
                                            <h5>{{ __('Total Tasks') }}</h5>
                                            <p class="acc-amount">{{$totalTask}}</p>
                                    </div>
                                </div>

                            </div>
                    </div> 
                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                            <div class="widget widget-account-invoice-one">

                                <div class="widget-heading">
                                </div>

                                <div class="widget-content">
                                    <div class="invoice-box">
                                            <h5>{{ __('Bugs') }}</h5>
                                            <p class="acc-amount">{{$totalBugs}}</p>
                                    </div>
                                </div>

                            </div>
                    </div> 
              </div>
            <!-- end row-->

            <div class="row">
                <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-chart-one">
                        <div class="widget-heading">
                            <h5 class="">Task Overview</h5>
                        </div>

                        <div class="widget-content">
                            <div class="tabs tab-content">
                                <div id="content_1" class="tabcontent"> 
                                    <div id="revenueMonthly"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-chart-two">
                        <div class="widget-heading">
                            <h5 class="">Project Status</h5>
                        </div>
                        <div class="widget-content">
                            <div id="chart-2" class=""></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row-->


            <div class="row">
              
              <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                  <div class="widget widget-table-two">

                            <div class="widget-heading">
                                <h5 class="">{{$completeTask}}</b> {{ __('Tasks completed out of')}} {{$totalTask}}</h5>
                            </div>

                            <div class="widget-content">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><div class="th-content">Name</div></th>
                                                <th><div class="th-content">Project</div></th>
                                                <th><div class="th-content">Date</div></th>
                                                <th><div class="th-content">Assigné à</div></th>
                                                <th><div class="th-content th-heading">Status</div></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($tasks as $task)
                                            <tr>
                                                <td><a href="{{route('projects.task.board',[$currantWorkspace->slug,$task->project_id])}}" class="text-body">{{$task->title}}</a></td>
                                                <td>{{$task->project->name}}</td>
                                                <td><div class="td-content product-brand">{{\App\Utility::get_timeago(strtotime($task->due_date))}}</div></td>
                                                <td><div class="td-content customer-name"><img wi @if($task->user->avatar) src="{{asset('/storage/avatars/'.$task->user->avatar)}}" class="profile-img" alt="avatar" @else avatar="{{ $task->user->name }}"@endif class="rounded-circle"></div></td>
                                                <td>
                                                          @if($task->status=='todo')
                                                            <span class="badge outline-badge-primary">{{__(ucwords($task->status))}}</span>
                                                          @elseif($task->status=='in progress')
                                                            <span class="badge outline-badge-warning">{{__(ucwords($task->status))}}</span>
                                                          @elseif($task->status=='review')
                                                            <span class="badge outline-badge-danger">{{__(ucwords($task->status))}}</span>
                                                          @elseif($task->status=='done')
                                                            <span class="badge outline-badge-success">{{__(ucwords($task->status))}}</span>
                                                          @endif
                                                </td>
                                            </tr>
                                          @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                  </div>
              </div>

              <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                  <div class="widget widget-table-two">

                            <div class="widget-heading">
                                <h5 class="">{{$completeBug}}</b> {{ __('Tasks completed out of')}} {{$totalBugs}}</h5>
                            </div>

                            <div class="widget-content">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><div class="th-content">Name</div></th>
                                                <th><div class="th-content">Project</div></th>
                                                <th><div class="th-content">Assigné à</div></th>
                                                <th><div class="th-content th-heading">Status</div></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($bugs as $bug)
                                            <tr>
                                                <td><a href="{{route('projects.bug.report',[$currantWorkspace->slug,$bug->project_id])}}" class="text-body">{{$bug->title}}</a></td>
                                                <td>{{$bug->project->name}}</td>
                                                <td><div class="td-content customer-name"><img wi @if($task->user->avatar) src="{{asset('/storage/avatars/'.$task->user->avatar)}}" class="profile-img" alt="avatar" @else avatar="{{ $task->user->name }}"@endif class="rounded-circle"></div></td>
                                                <td>
                                                    @if($bug->priority=="High")
                                                      <span class="badge outline-badge-danger">{{ __('High')}}</span>
                                                    @elseif($bug->priority=="Medium")
                                                      <span class="badge outline-badge-warning">{{ __('Medium')}}</span>
                                                    @else
                                                      <span class="badge outline-badge-info">{{ __('Low')}}</span>
                                                    @endif
                                                </td>
                                            </tr>
                                          @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                  </div>
              </div>

            </div>
            <!-- end row-->

    @endif
</div>
@endsection

@push('style')
    <link rel="stylesheet" href="{{asset('src/css/dashboard/dash_1.css')}}">
@endpush



@push('scripts')

    <!-- third party js -->
    <script src="{{ asset('plugins/apex/apexcharts.min.js') }}"></script>
    <!-- third party js ends -->

    @if(isset($currantWorkspace) && $currantWorkspace)
    <!-- demo app -->
    <script>

/*
    =================================
        Revenue Monthly | Options
    =================================
*/

var options1 = {
  chart: {
    fontFamily: 'Nunito, sans-serif',
    height: 365,
    type: 'area',
    zoom: {
        enabled: false
    },
    dropShadow: {
      enabled: true,
      opacity: 0.3,
      blur: 5,
      left: -7,
      top: 22
    },
    toolbar: {
      show: false
    },
    events: {
      mounted: function(ctx, config) {
        const highest1 = ctx.getHighestValueInSeries(0);
        const highest2 = ctx.getHighestValueInSeries(1);

        ctx.addPointAnnotation({
          x: new Date(ctx.w.globals.seriesX[0][ctx.w.globals.series[0].indexOf(highest1)]).getTime(),
          y: highest1,
          label: {
            style: {
              cssClass: 'd-none'
            }
          },
          customSVG: {
              SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#1b55e2" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
              cssClass: undefined,
              offsetX: -8,
              offsetY: 5
          }
        })

        ctx.addPointAnnotation({
          x: new Date(ctx.w.globals.seriesX[1][ctx.w.globals.series[1].indexOf(highest2)]).getTime(),
          y: highest2,
          label: {
            style: {
              cssClass: 'd-none'
            }
          },
          customSVG: {
              SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#e7515a" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
              cssClass: undefined,
              offsetX: -8,
              offsetY: 5
          }
        })
      },
    }
  },
  colors: ['#1b55e2', '#e7515a', '#0acf97', '#727cf5', '#875767'],
  dataLabels: {
      enabled: false
  },
  markers: {
    discrete: [{
    seriesIndex: 0,
    dataPointIndex: 7,
    fillColor: '#000',
    strokeColor: '#000',
    size: 5
  }, {
    seriesIndex: 2,
    dataPointIndex: 11,
    fillColor: '#000',
    strokeColor: '#000',
    size: 4
  }]
  },
  subtitle: {
    // text: 'Task Overview',
    align: 'left',
    margin: 0,
    offsetX: -10,
    offsetY: 35,
    floating: false,
    style: {
      fontSize: '14px',
      color:  '#888ea8'
    }
  },
  title: {
    text: 'Task',
    align: 'left',
    margin: 0,
    offsetX: -10,
    offsetY: 0,
    floating: false,
    style: {
      fontSize: '25px',
      color:  '#bfc9d4'
    },
  },
  stroke: {
      show: true,
      curve: 'smooth',
      width: 2,
      lineCap: 'square'
  },
  series: [{
      name: "{{ __('Todo') }}",
      data: {!! json_encode($chartData['todo']) !!}
  }, {
      name: "{{ __('In Progress') }}",
      data: {!! json_encode($chartData['progress']) !!}
  }, {
      name: "{{ __('Review') }}",
      data: {!! json_encode($chartData['review']) !!}
  }, {
      name: "{{ __('Done') }}",
      data: {!! json_encode($chartData['done']) !!}
  }],
  labels: {!! json_encode($chartData['label']) !!},
  xaxis: {
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    },
    crosshairs: {
      show: true
    },
    labels: {
      offsetX: 0,
      offsetY: 5,
      style: {
          fontSize: '12px',
          fontFamily: 'Nunito, sans-serif',
          cssClass: 'apexcharts-xaxis-title',
      },
    }
  },
  yaxis: {
    labels: {
      formatter: function(value, index) {
        return (value)
      },
      offsetX: -22,
      offsetY: 0,
      style: {
          fontSize: '12px',
          fontFamily: 'Nunito, sans-serif',
          cssClass: 'apexcharts-yaxis-title',
      },
    }
  },
  grid: {
    borderColor: '#191e3a',
    strokeDashArray: 5,
    xaxis: {
        lines: {
            show: true
        }
    },   
    yaxis: {
        lines: {
            show: false,
        }
    },
    padding: {
      top: 0,
      right: 0,
      bottom: 0,
      left: -10
    }, 
  }, 
  legend: {
    position: 'top',
    horizontalAlign: 'right',
    offsetY: -50,
    fontSize: '16px',
    fontFamily: 'Nunito, sans-serif',
    markers: {
      width: 10,
      height: 10,
      strokeWidth: 0,
      strokeColor: '#fff',
      fillColors: undefined,
      radius: 12,
      onClick: undefined,
      offsetX: 0,
      offsetY: 0
    },    
    itemMargin: {
      horizontal: 0,
      vertical: 20
    }
  },
  tooltip: {
    theme: 'dark',
    marker: {
      show: true,
    },
    x: {
      show: false,
    }
  },
  fill: {
      type:"gradient",
      gradient: {
          type: "vertical",
          shadeIntensity: 1,
          inverseColors: !1,
          opacityFrom: .28,
          opacityTo: .05,
          stops: [45, 100]
      }
  },
  responsive: [{
    breakpoint: 575,
    options: {
      legend: {
          offsetY: -30,
      },
    },
  }]
}

/*
    ==================================
        Sales By Category | Options
    ==================================
*/
var options = {
    chart: {
        type: 'donut',
        width: 380
    },
    colors: ['#5c1ac3', '#e2a03f', '#e7515a', '#e2a03f'],
    dataLabels: {
      enabled: false
    },
    legend: {
        position: 'bottom',
        horizontalAlign: 'center',
        fontSize: '14px',
        markers: {
          width: 10,
          height: 10,
        },
        itemMargin: {
          horizontal: 0,
          vertical: 8
        }
    },
    plotOptions: {
      pie: {
        donut: {
          size: '65%',
          background: 'transparent',
          labels: {
            show: true,
            name: {
              show: true,
              fontSize: '29px',
              fontFamily: 'Nunito, sans-serif',
              color: undefined,
              offsetY: -10
            },
            value: {
              show: true,
              fontSize: '26px',
              fontFamily: 'Nunito, sans-serif',
              color: '#bfc9d4',
              offsetY: 16,
              formatter: function (val) {
                return val
              }
            },
            total: {
              show: true,
              showAlways: true,
              label: 'Total',
              color: '#888ea8',
              formatter: function (w) {
                return w.globals.seriesTotals.reduce( function(a, b) {
                  return a + b
                })
              }
            }
          }
        }
      }
    },
    stroke: {
      show: true,
      width: 25,
      colors: '#0e1726'
    },
    series: {!! json_encode($arrProcessPer) !!},
    labels: {!! json_encode($arrProcessLable) !!},
    responsive: [{
        breakpoint: 1599,
        options: {
            chart: {
                width: '350px',
                height: '400px'
            },
            legend: {
                position: 'bottom'
            }
        },

        breakpoint: 1439,
        options: {
            chart: {
                width: '250px',
                height: '390px'
            },
            legend: {
                position: 'bottom'
            },
            plotOptions: {
              pie: {
                donut: {
                  size: '65%',
                }
              }
            }
        },
    }]
}


/*
    ================================
        Revenue Monthly | Render
    ================================
*/
var chart1 = new ApexCharts(
    document.querySelector("#revenueMonthly"),
    options1
);

chart1.render();

/*
    =================================
        Sales By Category | Render
    =================================
*/
var chart = new ApexCharts(
    document.querySelector("#chart-2"),
    options
);

chart.render();

    </script>
    @endif
@endpush
