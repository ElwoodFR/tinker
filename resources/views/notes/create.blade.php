<form class="pl-3 pr-3" method="post" action="{{ route('notes.store',$currantWorkspace->slug) }}">
    @csrf
    <div class="form-group">
        <label for="title">{{ __('Title') }}</label>
        <input class="form-control" type="text" id="title" name="title" required="" placeholder="{{ __('Title') }}">
    </div>
    <div class="form-group">
        <label for="description">{{ __('Description') }}</label>
        <textarea class="form-control" id="description" name="text" required></textarea>
    </div>
    <div class="form-group">
        <label for="color">{{ __('Color') }}</label>
        <select class="form-control" name="color" required>
            <option value="note-personal">{{ __('Personal') }}</option>
            <option value="all-notes">{{ __('All') }}</option>
            <option value="note-important">{{ __('Important') }}</option>
            <option value="note-work">{{ __('Work') }}</option>
            <option value="note-social">{{ __('Social') }}</option>
        </select>
    </div>
    <div class="form-group">
        <button class="btn btn-primary" type="submit">{{ __('Create Note') }}</button>
    </div>
</form>