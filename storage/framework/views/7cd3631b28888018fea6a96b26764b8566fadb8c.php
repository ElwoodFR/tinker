<?php $__env->startSection('content'); ?>

    <section class="section">

    <?php if($currantWorkspace): ?>
            <h2 class="section-title"><?php echo e(__('Projects')); ?></h2>
            <div class="row">
                <div class="col-12">
                    <?php if(empty(env('PUSHER_APP_ID')) || empty(env('PUSHER_APP_KEY')) || empty(env('PUSHER_APP_SECRET')) || empty(env('PUSHER_APP_CLUSTER'))): ?>
                        <div class="alert alert-warning"><i class="dripicons-warning"></i> <?php echo e(__('Please Add Pusher Detail in .env')); ?></div>
                    <?php endif; ?>
                    <?php if(empty(env('MAIL_DRIVER')) || empty(env('MAIL_HOST')) || empty(env('MAIL_PORT')) || empty(env('MAIL_USERNAME'))  || empty(env('MAIL_PASSWORD'))  || empty(env('MAIL_PASSWORD'))): ?>
                        <div class="alert alert-warning"><i class="dripicons-warning"></i> <?php echo e(__('Please Add Mail Setting Detail in .env')); ?></div>
                    <?php endif; ?>
                </div>
                <div class="col-12">
                    <div class="card widget-inline">
                        <div class="card-body p-0">
                            <div class="row no-gutters">
                                <div class="col-sm-6 col-xl-3 animated">
                                    <div class="card shadow-none m-0">
                                        <div class="card-body text-center">
                                            <i class="dripicons-briefcase text-muted" style="font-size: 24px;"></i>
                                            <h3><span><?php echo e($totalProject); ?></span></h3>
                                            <p class="text-muted font-15 mb-0"><?php echo e(__('Total Projects')); ?></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xl-3 animated">
                                    <div class="card shadow-none m-0 border-left">
                                        <div class="card-body text-center">
                                            <i class="dripicons-checklist text-muted" style="font-size: 24px;"></i>
                                            <h3><span><?php echo e($totalTask); ?></span></h3>
                                            <p class="text-muted font-15 mb-0"><?php echo e(__('Total Tasks')); ?></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xl-3 animated">
                                    <div class="card shadow-none m-0 border-left">
                                        <div class="card-body text-center">
                                            <i class="dripicons-user-group text-muted" style="font-size: 24px;"></i>
                                            <h3><span><?php echo e($totalMembers); ?></span></h3>
                                            <p class="text-muted font-15 mb-0"><?php echo e(__('Members')); ?></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xl-3 animated">
                                    <div class="card shadow-none m-0 border-left">
                                        <div class="card-body text-center">
                                            <i class="mdi mdi-bug-outline text-muted" style="font-size: 24px;"></i>
                                            <h3><span><?php echo e($totalBugs); ?></span></h3>
                                            <p class="text-muted font-15 mb-0"><?php echo e(__('Bugs')); ?></p>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- end row -->
                        </div>
                    </div> <!-- end card-box-->
                </div> <!-- end col-->
            </div>
            <!-- end row-->

            <div class="row">
                <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-chart-one">
                        <div class="widget-heading">
                            <h5 class="">Task Overview</h5>
                        </div>

                        <div class="widget-content">
                            <div class="tabs tab-content">
                                <div id="content_1" class="tabcontent"> 
                                    <div id="revenueMonthly"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-chart-two">
                        <div class="widget-heading">
                            <h5 class="">Project Status</h5>
                        </div>
                        <div class="widget-content">
                            <div id="chart-2" class=""></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row-->


            <div class="row">

                <div class="col-xl-8">
                    <div class="card animated">
                        <div class="card-header">
                            <h4><?php echo e(__('Tasks')); ?></h4>
                        </div>
                        <div class="card-body">

                            <p><b><?php echo e($completeTask); ?></b> <?php echo e(__('Tasks completed out of')); ?> <?php echo e($totalTask); ?></p>

                            <div class="table-responsive">
                                <table class="table table-centered table-hover mb-0 animated">
                                    <tbody>
                                    <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td>
                                                <div class="font-14 my-1"><a href="<?php echo e(route('projects.task.board',[$currantWorkspace->slug,$task->project_id])); ?>" class="text-body"><?php echo e($task->title); ?></a></div>
                                                <span class="text-muted font-13"><?php echo e(__('Due in')); ?> <?php echo e(\App\Utility::get_timeago(strtotime($task->due_date))); ?></span>
                                            </td>
                                            <td>
                                                <span class="text-muted font-13"><?php echo e(__('Status')); ?></span> <br/>
                                                <?php if($task->status=='todo'): ?>
                                                    <span class="badge badge-primary"><?php echo e(__(ucwords($task->status))); ?></span>
                                                <?php elseif($task->status=='in progress'): ?>
                                                    <span class="badge badge-warning"><?php echo e(__(ucwords($task->status))); ?></span>
                                                <?php elseif($task->status=='review'): ?>
                                                    <span class="badge badge-danger"><?php echo e(__(ucwords($task->status))); ?></span>
                                                <?php elseif($task->status=='done'): ?>
                                                    <span class="badge badge-success"><?php echo e(__(ucwords($task->status))); ?></span>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <span class="text-muted font-13"><?php echo e(__('Project')); ?></span>
                                                <div class="font-14 mt-1 font-weight-normal"><?php echo e($task->project->name); ?></div>
                                            </td>
                                            <?php if($currantWorkspace->permission == 'Owner' || Auth::user()->getGuard() == 'client'): ?>
                                            <td>
                                                <span class="text-muted font-13"><?php echo e(__('Assigned to')); ?></span>
                                                <div class="font-14 mt-1 font-weight-normal"><?php echo e($task->user->name); ?></div>
                                            </td>
                                            <?php endif; ?>

                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div> <!-- end table-responsive-->

                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            <!-- end row-->

    <?php endif; ?>
    </section>
    <!-- container -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('style'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('src/css/dashboard/dash_1.css')); ?>">
<?php $__env->stopPush(); ?>



<?php $__env->startPush('scripts'); ?>

    <!-- third party js -->
    <script src="<?php echo e(asset('plugins/apex/apexcharts.min.js')); ?>"></script>
    <!-- third party js ends -->

    <?php if(isset($currantWorkspace) && $currantWorkspace): ?>
    <!-- demo app -->
    <script>

/*
    =================================
        Revenue Monthly | Options
    =================================
*/

var options1 = {
  chart: {
    fontFamily: 'Nunito, sans-serif',
    height: 365,
    type: 'area',
    zoom: {
        enabled: false
    },
    dropShadow: {
      enabled: true,
      opacity: 0.3,
      blur: 5,
      left: -7,
      top: 22
    },
    toolbar: {
      show: false
    },
    events: {
      mounted: function(ctx, config) {
        const highest1 = ctx.getHighestValueInSeries(0);
        const highest2 = ctx.getHighestValueInSeries(1);

        ctx.addPointAnnotation({
          x: new Date(ctx.w.globals.seriesX[0][ctx.w.globals.series[0].indexOf(highest1)]).getTime(),
          y: highest1,
          label: {
            style: {
              cssClass: 'd-none'
            }
          },
          customSVG: {
              SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#1b55e2" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
              cssClass: undefined,
              offsetX: -8,
              offsetY: 5
          }
        })

        ctx.addPointAnnotation({
          x: new Date(ctx.w.globals.seriesX[1][ctx.w.globals.series[1].indexOf(highest2)]).getTime(),
          y: highest2,
          label: {
            style: {
              cssClass: 'd-none'
            }
          },
          customSVG: {
              SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#e7515a" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
              cssClass: undefined,
              offsetX: -8,
              offsetY: 5
          }
        })
      },
    }
  },
  colors: ['#1b55e2', '#e7515a', '#0acf97', '#727cf5', '#875767'],
  dataLabels: {
      enabled: false
  },
  markers: {
    discrete: [{
    seriesIndex: 0,
    dataPointIndex: 7,
    fillColor: '#000',
    strokeColor: '#000',
    size: 5
  }, {
    seriesIndex: 2,
    dataPointIndex: 11,
    fillColor: '#000',
    strokeColor: '#000',
    size: 4
  }]
  },
  subtitle: {
    // text: 'Task Overview',
    align: 'left',
    margin: 0,
    offsetX: -10,
    offsetY: 35,
    floating: false,
    style: {
      fontSize: '14px',
      color:  '#888ea8'
    }
  },
  title: {
    text: 'Task',
    align: 'left',
    margin: 0,
    offsetX: -10,
    offsetY: 0,
    floating: false,
    style: {
      fontSize: '25px',
      color:  '#bfc9d4'
    },
  },
  stroke: {
      show: true,
      curve: 'smooth',
      width: 2,
      lineCap: 'square'
  },
  series: [{
      name: "<?php echo e(__('Todo')); ?>",
      data: <?php echo json_encode($chartData['todo']); ?>

  }, {
      name: "<?php echo e(__('In Progress')); ?>",
      data: <?php echo json_encode($chartData['progress']); ?>

  }, {
      name: "<?php echo e(__('Review')); ?>",
      data: <?php echo json_encode($chartData['review']); ?>

  }, {
      name: "<?php echo e(__('Done')); ?>",
      data: <?php echo json_encode($chartData['done']); ?>

  }],
  labels: <?php echo json_encode($chartData['label']); ?>,
  xaxis: {
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    },
    crosshairs: {
      show: true
    },
    labels: {
      offsetX: 0,
      offsetY: 5,
      style: {
          fontSize: '12px',
          fontFamily: 'Nunito, sans-serif',
          cssClass: 'apexcharts-xaxis-title',
      },
    }
  },
  yaxis: {
    labels: {
      formatter: function(value, index) {
        return (value)
      },
      offsetX: -22,
      offsetY: 0,
      style: {
          fontSize: '12px',
          fontFamily: 'Nunito, sans-serif',
          cssClass: 'apexcharts-yaxis-title',
      },
    }
  },
  grid: {
    borderColor: '#191e3a',
    strokeDashArray: 5,
    xaxis: {
        lines: {
            show: true
        }
    },   
    yaxis: {
        lines: {
            show: false,
        }
    },
    padding: {
      top: 0,
      right: 0,
      bottom: 0,
      left: -10
    }, 
  }, 
  legend: {
    position: 'top',
    horizontalAlign: 'right',
    offsetY: -50,
    fontSize: '16px',
    fontFamily: 'Nunito, sans-serif',
    markers: {
      width: 10,
      height: 10,
      strokeWidth: 0,
      strokeColor: '#fff',
      fillColors: undefined,
      radius: 12,
      onClick: undefined,
      offsetX: 0,
      offsetY: 0
    },    
    itemMargin: {
      horizontal: 0,
      vertical: 20
    }
  },
  tooltip: {
    theme: 'dark',
    marker: {
      show: true,
    },
    x: {
      show: false,
    }
  },
  fill: {
      type:"gradient",
      gradient: {
          type: "vertical",
          shadeIntensity: 1,
          inverseColors: !1,
          opacityFrom: .28,
          opacityTo: .05,
          stops: [45, 100]
      }
  },
  responsive: [{
    breakpoint: 575,
    options: {
      legend: {
          offsetY: -30,
      },
    },
  }]
}

/*
    ==================================
        Sales By Category | Options
    ==================================
*/
var options = {
    chart: {
        type: 'donut',
        width: 380
    },
    colors: ['#5c1ac3', '#e2a03f', '#e7515a', '#e2a03f'],
    dataLabels: {
      enabled: false
    },
    legend: {
        position: 'bottom',
        horizontalAlign: 'center',
        fontSize: '14px',
        markers: {
          width: 10,
          height: 10,
        },
        itemMargin: {
          horizontal: 0,
          vertical: 8
        }
    },
    plotOptions: {
      pie: {
        donut: {
          size: '65%',
          background: 'transparent',
          labels: {
            show: true,
            name: {
              show: true,
              fontSize: '29px',
              fontFamily: 'Nunito, sans-serif',
              color: undefined,
              offsetY: -10
            },
            value: {
              show: true,
              fontSize: '26px',
              fontFamily: 'Nunito, sans-serif',
              color: '#bfc9d4',
              offsetY: 16,
              formatter: function (val) {
                return val
              }
            },
            total: {
              show: true,
              showAlways: true,
              label: 'Total',
              color: '#888ea8',
              formatter: function (w) {
                return w.globals.seriesTotals.reduce( function(a, b) {
                  return a + b
                })
              }
            }
          }
        }
      }
    },
    stroke: {
      show: true,
      width: 25,
      colors: '#0e1726'
    },
    series: <?php echo json_encode($arrProcessPer); ?>,
    labels: <?php echo json_encode($arrProcessLable); ?>,
    responsive: [{
        breakpoint: 1599,
        options: {
            chart: {
                width: '350px',
                height: '400px'
            },
            legend: {
                position: 'bottom'
            }
        },

        breakpoint: 1439,
        options: {
            chart: {
                width: '250px',
                height: '390px'
            },
            legend: {
                position: 'bottom'
            },
            plotOptions: {
              pie: {
                donut: {
                  size: '65%',
                }
              }
            }
        },
    }]
}


/*
    ================================
        Revenue Monthly | Render
    ================================
*/
var chart1 = new ApexCharts(
    document.querySelector("#revenueMonthly"),
    options1
);

chart1.render();

/*
    =================================
        Sales By Category | Render
    =================================
*/
var chart = new ApexCharts(
    document.querySelector("#chart-2"),
    options
);

chart.render();

    </script>
    <?php endif; ?>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/kevindupas/Documents/Dev/projectTinker/resources/views/home.blade.php ENDPATH**/ ?>