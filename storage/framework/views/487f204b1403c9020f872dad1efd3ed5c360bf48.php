<?php $__env->startSection('content'); ?>
<div class="row app-notes layout-top-spacing" id="cancel-row">
    <div class="col-lg-12">
        <?php if($projects && $currantWorkspace): ?>
        <div class="row mb-2">
            <div class="col-sm-4">
                <?php if(auth()->guard('web')->check()): ?>
                <?php if($currantWorkspace->creater->id == Auth::user()->id): ?>
                <button type="button" class="btn btn-primary" data-ajax-popup="true" data-size="lg"
                    data-title="<?php echo e(__('Create New Project')); ?>"
                    data-url="<?php echo e(route('projects.create',$currantWorkspace->slug)); ?>">
                    <i class="mdi mdi-plus"></i> <?php echo e(__('Create Project')); ?>

                </button>
                <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="col-sm-8">
                <div class="text-sm-right status-filter">
                    <div class="btn-group mb-3">
                        <button type="button" class="btn btn-primary" data-status="All"><?php echo e(__('All')); ?></button>
                    </div>
                    <div class="btn-group mb-3 ml-1">
                        <button type="button" class="btn btn-light" data-status="Ongoing"><?php echo e(__('Ongoing')); ?></button>
                        <button type="button" class="btn btn-light" data-status="Finished"><?php echo e(__('Finished')); ?></button>
                        <button type="button" class="btn btn-light" data-status="OnHold"><?php echo e(__('OnHold')); ?></button>
                    </div>

                </div>
            </div><!-- end col-->
        </div>
        <div class="row">
            <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-sm-4">
                <div class="card animated filter <?php echo e($project->status); ?>">
                    <div class="card-body">

                        <div class="progress-order">
                            <div class="progress-order-header">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        <?php if($project->is_active): ?>
                                        <a href="<?php if(auth()->guard('web')->check()): ?><?php echo e(route('projects.show',[$currantWorkspace->slug,$project->id])); ?><?php elseif(auth()->guard()->check()): ?><?php echo e(route('client.projects.show',[$currantWorkspace->slug,$project->id])); ?><?php endif; ?>"
                                            title="<?php echo e($project->name); ?>" class="text-title"><?php echo e($project->name); ?></a>
                                        <?php else: ?>
                                        <a href="#" title="<?php echo e(__('Locked')); ?>"
                                            class="text-title"><?php echo e($project->name); ?></a>
                                        <?php endif; ?>
                                        <?php if(!$project->is_active): ?>
                                        <a href="#" class="btn" title="<?php echo e(__('Locked')); ?>">
                                            lock
                                        </a>
                                        <?php else: ?>
                                        <?php if(auth()->guard('web')->check()): ?>
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">
                                            <svg viewBox="0 0 24 24" width="16" height="16" stroke="currentColor"
                                                stroke-width="2" fill="none" stroke-linecap="round"
                                                stroke-linejoin="round" class="css-i6dzq1" style="
                                                margin-top: -3px;
                                                margin-left: 5px;
                                            ">
                                                <circle cx="12" cy="12" r="3"></circle>
                                                <path
                                                    d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z">
                                                </path>
                                            </svg>
                                        </a>
                                        <div class="dropdown card-widgets">
                                            <div class="dropdown-menu dropdown-menu-right">

                                                <?php if($currantWorkspace->permission == 'Owner'): ?>
                                                <a href="#" class="dropdown-item" data-ajax-popup="true" data-size="lg"
                                                    data-title="<?php echo e(__('Edit Project')); ?>"
                                                    data-url="<?php echo e(route('projects.edit',[$currantWorkspace->slug,$project->id])); ?>"><i
                                                        class="mdi mdi-pencil mr-1"></i><?php echo e(__('Edit')); ?></a>
                                                <a href="#"
                                                    onclick="(confirm('<?php echo e(__('Are you sure ?')); ?>')?document.getElementById('delete-form-<?php echo e($project->id); ?>').submit(): '');"
                                                    class="dropdown-item"><i
                                                        class="mdi mdi-delete mr-1"></i><?php echo e(__('Delete')); ?></a>
                                                <form id="delete-form-<?php echo e($project->id); ?>"
                                                    action="<?php echo e(route('projects.destroy',[$currantWorkspace->slug,$project->id])); ?>"
                                                    method="POST" style="display: none;">
                                                    <?php echo csrf_field(); ?>
                                                    <?php echo method_field('DELETE'); ?>
                                                </form>
                                                <a href="#" class="dropdown-item" class="dropdown-item"
                                                    data-ajax-popup="true" data-size="lg"
                                                    data-title="<?php echo e(__('Invite Users')); ?>"
                                                    data-url="<?php echo e(route('projects.invite.popup',[$currantWorkspace->slug,$project->id])); ?>"><i
                                                        class="mdi mdi-email-outline mr-1"></i><?php echo e(__('Invite')); ?></a>
                                                <a href="#" class="dropdown-item" data-ajax-popup="true" data-size="lg"
                                                    data-title="<?php echo e(__('Share to Clients')); ?>"
                                                    data-url="<?php echo e(route('projects.share.popup',[$currantWorkspace->slug,$project->id])); ?>"><i
                                                        class="mdi mdi-email-outline mr-1"></i><?php echo e(__('Share')); ?></a>
                                                <?php else: ?>
                                                <a href="#"
                                                    onclick="(confirm('<?php echo e(__('Are you sure ?')); ?>')?document.getElementById('leave-form-<?php echo e($project->id); ?>').submit(): '');"
                                                    class="dropdown-item"><i
                                                        class="mdi mdi-exit-to-app mr-1"></i><?php echo e(__('Leave')); ?></a>
                                                <form id="leave-form-<?php echo e($project->id); ?>"
                                                    action="<?php echo e(route('projects.leave',[$currantWorkspace->slug,$project->id])); ?>"
                                                    method="POST" style="display: none;">
                                                    <?php echo csrf_field(); ?>
                                                    <?php echo method_field('DELETE'); ?>
                                                </form>
                                                <?php endif; ?>

                                            </div>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        <?php if($project->status == 'Finished'): ?>
                                        <span class="shadow-none badge badge-success"><?php echo e(__('Finished')); ?></span>
                                        <?php elseif($project->status == 'Ongoing'): ?>
                                        <span class="shadow-none badge badge-info"><?php echo e(__('Ongoing')); ?></span>
                                        <?php else: ?>
                                        <span class="shadow-none badge badge-warning"><?php echo e(__('OnHold')); ?></span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="progress-order-body">
                                <div class="row mt-4">
                                    <div class="col-md-12 text-left">
                                        <div class="author-box-description">
                                            <p>
                                                <?php echo e(Str::limit($project->description, $limit = 50, $end = '...')); ?>

                                            </p>
                                        </div>
                                        <p class="mb-1">
                                            <span class="pr-2 text-nowrap mb-2 d-inline-block">
                                                <i class="mdi mdi-format-list-bulleted-type text-muted"></i>
                                                <b><?php echo e($project->countTask()); ?></b> <?php echo e(__('Tasks')); ?>

                                            </span>
                                            <span class="text-nowrap mb-2 d-inline-block">
                                                <i class="mdi mdi-comment-multiple-outline text-muted"></i>
                                                <b><?php echo e($project->countTaskComments()); ?></b> <?php echo e(__('Comments')); ?>

                                            </span>
                                        </p>
                                    </div>
                                    <div class="col-md-12">
                                        <ul class="list-inline badge-collapsed-img mb-0 mb-3">
                                            <?php $__currentLoopData = $project->users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($user->pivot->is_active): ?>
                                            <li class="list-inline-item chat-online-usr">
                                                <img <?php if($user->avatar): ?>
                                                src="<?php echo e(asset('/storage/avatars/'.$user->avatar)); ?>" <?php else: ?>
                                                avatar="<?php echo e($user->name); ?>" <?php endif; ?> class="rounded-circle">
                                            </li>
                                            <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <?php else: ?>
        <div class="container mt-5">
            <div class="page-error">
                <div class="page-inner">
                    <h1>404</h1>
                    <div class="page-description">
                        <?php echo e(__('Page Not Found')); ?>

                    </div>
                    <div class="page-search">
                        <p class="text-muted mt-3">
                            <?php echo e(__('It\'s looking like you may have taken a wrong turn. Don\'t worry... it happens to the best of us. Here\'s a little tip that might help you get back on track.')); ?>

                        </p>
                        <div class="mt-3">
                            <a class="btn btn-info mt-3" href="<?php echo e(route('home')); ?>"><i class="mdi mdi-reply"></i>
                                <?php echo e(__('Return Home')); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php endif; ?>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('style'); ?>

<link href="<?php echo e(asset('assets/css/vendor/bootstrap-tagsinput.css')); ?>" rel="stylesheet">
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/kevindupas/Documents/Dev/Tinker/resources/views/projects/index.blade.php ENDPATH**/ ?>