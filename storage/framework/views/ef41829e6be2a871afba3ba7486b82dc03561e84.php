<header class="header navbar navbar-expand-sm">

    <ul class="navbar-item theme-brand flex-row  text-center">
        <li class="nav-item theme-logo">
            <a href="/">
                <img src="<?php echo e(asset('storage/logo/pni.png')); ?>" class="navbar-logo" alt="logo">
            </a>
        </li>
    </ul>
  
    <ul class="navbar-item flex-row ml-md-0 ml-auto">
        <li class="nav-item align-self-center search-animated">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                class="feather feather-search toggle-search">
                <circle cx="11" cy="11" r="8"></circle>
                <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
            </svg>
            <form class="form-inline search-full form-inline search" role="search">
                <div class="search-bar">
                    <input type="text" class="form-control search-form-control  ml-lg-auto" placeholder="Search...">
                </div>
            </form>
        </li>
    </ul>
  
    <ul class="navbar-item flex-row ml-md-auto">
  
        <?php if(isset($currantWorkspace) && $currantWorkspace && $currantWorkspace->permission == 'Owner'): ?>
        <?php
          $currantLang = basename(App::getLocale());
        ?>
        <li class="nav-item dropdown language-dropdown">
            <a href="javascript:void(0);" class="nav-link dropdown-toggle" id="language-dropdown" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <?php echo e(Str::upper($currantLang)); ?>

            </a>
            <div class="dropdown-menu position-absolute" aria-labelledby="language-dropdown">
                <?php $__currentLoopData = $currantWorkspace->languages(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($currantLang != $lang): ?>
                <!-- item-->
                <a href="<?php echo e(route('change_lang_workspace',[$currantWorkspace->id,$lang])); ?>" class="dropdown-item">
                    <span class="align-middle"><?php echo e(Str::upper($lang)); ?></span>
                </a>
                <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <a href="<?php echo e(route('lang_workspace',[$currantWorkspace->slug,$currantWorkspace->lang])); ?>"
                    class="dropdown-item notify-item">
                    <span class="align-middle"><?php echo e(__('Create & Customize')); ?></span>
                </a>
            </div>
        </li>
        <?php endif; ?>

        <?php if(isset($currantWorkspace) && $currantWorkspace): ?>
        <?php if(auth()->guard('web')->check()): ?>
        <li class="nav-item dropdown message-dropdown">
            <a href="javascript:void(0);" class="nav-link dropdown-toggle" id="messageDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
            </a>
            <div class="dropdown-menu p-0 position-absolute" aria-labelledby="messageDropdown">
                <div class="">

                    <a class="dropdown-item">
                        <div class="">
    
                            <div class="media">
                                <div class="dropdown-menu dropdown-list dropdown-menu-right">
                                    <div class="dropdown-header"><?php echo e(__('Messages')); ?>

                                        <div class="float-right">
                                            <a href="#" class="mark_all_as_read_message"><?php echo e(__('Mark All As Read')); ?></a>
                                        </div>
                                    </div>
                                    <div class="dropdown-list-content dropdown-list-message" tabindex="3">
                        
                                    </div>
                                    <div class="dropdown-footer text-center">
                                        <a href="<?php echo e(route('chats.index',$currantWorkspace->slug)); ?>"><?php echo e(__('View All')); ?> <i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </a>
                </div>
            </div>
        </li>
    
        <li class="nav-item dropdown notification-dropdown">
            <?php
                $notifications = Auth::user()->notifications($currantWorkspace->id)
            ?>
            <a href="javascript:void(0);" class="nav-link dropdown-toggle" id="notificationDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg>
                <span class="<?php if(count($notifications)): ?> badge badge-success beep <?php endif; ?>""></span>
            </a>
            <div class="dropdown-menu position-absolute" aria-labelledby="notificationDropdown">
                <div class="float-right">
                    <a href="#" class="mark_all_as_read"><?php echo e(__('Mark All As Read')); ?></a>
                </div>
                <div class="notification-scroll">
                    <?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notification): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="dropdown-item">
                        <div class="media">
                            <div class="media-body">
                                <div class="notification-para"><span class="user-name">
                                    <?php echo $notification->toHtml(); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                </div>
            </div>
        </li>
    
        <?php endif; ?>
        <?php endif; ?>
  
        <li class="nav-item dropdown user-profile-dropdown">
            <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <img <?php if(Auth::user()->avatar): ?> src="<?php echo e(asset('/storage/avatars/'.Auth::user()->avatar)); ?>" <?php else: ?>
                avatar="<?php echo e(Auth::user()->name); ?>" <?php endif; ?> alt="user-image" class="rounded-circle mr-1"></a>
            </a>
            <div class="dropdown-menu position-absolute" aria-labelledby="userProfileDropdown">
                <div class="">
                    <div class="dropdown-item">
                        <?php $__currentLoopData = Auth::user()->workspace; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workspace): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <a href="<?php if($currantWorkspace->id == $workspace->id): ?>#<?php else: ?><?php echo e(route('change_workspace',$workspace->id)); ?><?php endif; ?>"
                            title="<?php echo e($workspace->name); ?>" class="dropdown-item notify-item">
                            <?php if($currantWorkspace->id == $workspace->id): ?>
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>
                            <?php endif; ?>
                            <?php echo e($workspace->name); ?>

                              <?php if(isset($workspace->pivot->permission)): ?>
                                <?php if($workspace->pivot->permission =='Owner'): ?>
                                <span class="badge badge-primary"><?php echo e($workspace->pivot->permission); ?></span>
                                <?php else: ?>
                              <span class="badge badge-secondary"><?php echo e(__('Shared')); ?></span>
                              <?php endif; ?>
                            <?php endif; ?>
                        </a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <?php if(isset($currantWorkspace) && $currantWorkspace): ?>
                    <div class="dropdown-divider"></div>
                    <?php endif; ?>
                    <div class="dropdown-item">
                        <a href="#" class="dropdown-item notify-item" data-toggle="modal"
                            data-target="#modelCreateWorkspace">
                            <i class="mdi mdi-plus"></i>
                            <span><?php echo e(__('Create New Workspace')); ?></span>
                        </a>
                    </div>
                    <div class="dropdown-item">
                        <?php if(isset($currantWorkspace) && $currantWorkspace): ?>
                          <?php if(Auth::user()->id == $currantWorkspace->created_by): ?>
                          <a href="#" class="dropdown-item notify-item"
                              onclick="(confirm('Are you sure ?')?document.getElementById('remove-workspace-form').submit(): '');">
                              <i class=" mdi mdi-delete-outline"></i>
                              <span><?php echo e(__('Remove Me From This Workspace')); ?></span>
                          </a>
                          <form id="remove-workspace-form"
                              action="<?php echo e(route('delete_workspace', ['id' => $currantWorkspace->id])); ?>" method="POST"
                              style="display: none;">
                              <?php echo csrf_field(); ?>
                              <?php echo method_field('DELETE'); ?>
                          </form>
                          <?php else: ?>
                          <a href="#" class="dropdown-item notify-item"
                              onclick="(confirm('Are you sure ?')?document.getElementById('remove-workspace-form').submit(): '');">
                              <i class=" mdi mdi-delete-outline"></i>
                              <span><?php echo e(__('Leave Me From This Workspace')); ?></span>
                          </a>
                          <form id="remove-workspace-form"
                              action="<?php echo e(route('leave_workspace', ['id' => $currantWorkspace->id])); ?>" method="POST"
                              style="display: none;">
                              <?php echo csrf_field(); ?>
                              <?php echo method_field('DELETE'); ?>
                          </form>
                          <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="dropdown-item">
                        <a href="<?php echo e(route('users.my.account')); ?>" class="dropdown-item has-icon">
                            <i class="mdi mdi-account-circle mr-1"></i> <?php echo e(__('My Account')); ?>

                        </a>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="dropdown-item">
                        <a href="<?php echo e(route('logout')); ?>" class="dropdown-item has-icon text-danger"
                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="mdi mdi-logout mr-1"></i> <?php echo e(__('Logout')); ?>

                        </a>
                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                            <?php echo csrf_field(); ?>
                        </form>
                    </div>
  
                </div>
            </div>
        </li>
  
    </ul>
  </header><?php /**PATH /Users/kevindupas/Documents/Dev/projectTinker/resources/views/partials/topnav.blade.php ENDPATH**/ ?>