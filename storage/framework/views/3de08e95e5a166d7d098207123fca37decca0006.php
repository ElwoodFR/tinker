<?php $__env->startSection('content'); ?>
    <section class="section">
        <?php if($project && $currantWorkspace): ?>

            <?php if(auth()->guard('client')->check()): ?>
                <?php
                $permisions = Auth::user()->getPermission($project->id);
                ?>
            <?php endif; ?>

            <div class="row mb-2">
                <div class="col-sm-4">
                    <h2 class="section-title" style="margin-top: 10%;"></h2>
                </div>
                <div class="col-sm-8">
                    <div class="text-sm-right">
                        <div class="mt-4 mb-4" >
                            
                            <?php if(auth()->guard('client')->check()): ?>
                                <?php if(isset($permisions) && in_array('show task',$permisions)): ?>
                                    <a href="<?php echo e(route('client.projects.task.board',[$currantWorkspace->slug,$project->id])); ?>" class="btn btn-primary"><?php echo e(__('Task Board')); ?></a>
                                <?php endif; ?>
                            <?php elseif(auth()->guard()->check()): ?>
                                <a href="<?php echo e(route('projects.task.board',[$currantWorkspace->slug,$project->id])); ?>" class="btn btn-primary"><?php echo e(__('Task Board')); ?></a>
                            <?php endif; ?>
                            
                            <?php if(auth()->guard('client')->check()): ?>
                                <?php if(isset($permisions) && in_array('show bug report',$permisions)): ?>
                                    <a href="<?php echo e(route('client.projects.bug.report',[$currantWorkspace->slug,$project->id])); ?>" class="btn btn-primary"><?php echo e(__('Bug Report')); ?></a>
                                <?php endif; ?>
                            <?php elseif(auth()->guard()->check()): ?>
                                <a href="<?php echo e(route('projects.bug.report',[$currantWorkspace->slug,$project->id])); ?>" class="btn btn-primary"><?php echo e(__('Bug Report')); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 animated">
                    <!-- project card -->
                    <div class="card author-box card-primary">
                        <div class="card-body">
                            <div class="progress-order-header">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        <?php echo e($project->name); ?>

                                            <?php if(!$project->is_active): ?>
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                aria-expanded="false">
                                                lock
                                            </a>
                                            <?php else: ?>
                                                <?php if(auth()->guard('web')->check()): ?>
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                    aria-expanded="false">
                                                    <svg viewBox="0 0 24 24" width="16" height="16" stroke="currentColor"
                                                        stroke-width="2" fill="none" stroke-linecap="round"
                                                        stroke-linejoin="round" class="css-i6dzq1" style="
                                                        margin-top: -3px;
                                                        margin-left: 5px;">
                                                        <circle cx="12" cy="12" r="3"></circle>
                                                        <path
                                                            d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z">
                                                        </path>
                                                    </svg>
                                                </a>
                                                <div class="dropdown card-widgets">
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <?php if($currantWorkspace->permission == 'Owner'): ?>
                                                            <a href="#" class="dropdown-item" data-ajax-popup="true" data-size="lg" data-title="<?php echo e(__('Edit Project')); ?>" data-url="<?php echo e(route('projects.edit',[$currantWorkspace->slug,$project->id])); ?>"><i class="mdi mdi-pencil mr-1"></i><?php echo e(__('Edit')); ?></a>
                                                            <a href="#" onclick="(confirm('<?php echo e(__('Are you sure ?')); ?>')?document.getElementById('delete-form-<?php echo e($project->id); ?>').submit(): '');" class="dropdown-item"><i class="mdi mdi-delete mr-1"></i><?php echo e(__('Delete')); ?></a>
                                                            <form id="delete-form-<?php echo e($project->id); ?>" action="<?php echo e(route('projects.destroy',[$currantWorkspace->slug,$project->id])); ?>" method="POST" style="display: none;">
                                                                <?php echo csrf_field(); ?>
                                                                <?php echo method_field('DELETE'); ?>
                                                            </form>
                                                        <?php else: ?>
                                                            <a href="#" onclick="(confirm('<?php echo e(__('Are you sure ?')); ?>')?document.getElementById('leave-form-<?php echo e($project->id); ?>').submit(): '');" class="dropdown-item"><i class="mdi mdi-exit-to-app mr-1"></i><?php echo e(__('Leave')); ?></a>
                                                            <form id="leave-form-<?php echo e($project->id); ?>" action="<?php echo e(route('projects.leave',[$currantWorkspace->slug,$project->id])); ?>" method="POST" style="display: none;">
                                                                <?php echo csrf_field(); ?>
                                                                <?php echo method_field('DELETE'); ?>
                                                            </form>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        <?php if($project->status == 'Finished'): ?>
                                            <div class="badge badge-success"><?php echo e(__('Finished')); ?></div>
                                        <?php elseif($project->status == 'Ongoing'): ?>
                                            <div class="badge badge-secondary"><?php echo e(__('Ongoing')); ?></div>
                                        <?php else: ?>
                                            <div class="badge badge-warning"><?php echo e(__('OnHold')); ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-4 font-weight-bold"><?php echo e(__('Project Overview')); ?>:</div>

                            <div class="author-box-description">
                                <?php echo e($project->description); ?>

                            </div>

                            <div class="row mt-3">
                                <?php if($project->start_date): ?>
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <div class="font-weight-bold"><?php echo e(__('Start Date')); ?></div>
                                            <p><?php echo e(Utility::dateFormat($project->start_date)); ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if($project->end_date): ?>
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <div class="font-weight-bold"><?php echo e(__('End Date')); ?></div>
                                            <p><?php echo e(Utility::dateFormat($project->end_date)); ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="col-md-4">
                                    <div class="mb-4">
                                        <div class="font-weight-bold"><?php echo e(__('Budget')); ?></div>
                                        <p>$<?php echo e(number_format($project->budget)); ?></p>
                                    </div>
                                </div>
                            </div>

                        </div> <!-- end card-body-->

                    </div> <!-- end card-->

                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-one">

                                    <div class="widget-heading">
                                    </div>

                                    <div class="widget-content">
                                        <div class="invoice-box">
                                                <h5><?php echo e(__('Days left')); ?></h5>
                                                <p class="acc-amount">
                                                <?php
                                                    $datetime1 = new DateTime($project->end_date);
                                                    $datetime2 = new DateTime(date('Y-m-d'));
                                                    $interval = $datetime1->diff($datetime2);
                                                    $days = $interval->format('%a')
                                                ?>
                                                <?php echo e($days); ?>

                                                </p>
                                        </div>
                                    </div>

                                </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-one">

                                    <div class="widget-heading">
                                    </div>

                                    <div class="widget-content">
                                        <div class="invoice-box">
                                                <h5><?php echo e(__('Total task')); ?></h5>
                                                <p class="acc-amount">
                                                    <?php echo e($project->countTask()); ?>

                                                </p>
                                        </div>
                                    </div>

                                </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-one">

                                    <div class="widget-heading">
                                    </div>

                                    <div class="widget-content">
                                        <div class="invoice-box">
                                                <h5><?php echo e(__('Total bug')); ?></h5>
                                                <p class="acc-amount">
                                                    <?php echo e($project->countBugReport()); ?>

                                                </p>
                                        </div>
                                    </div>

                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-6 col-lg-12 col-md-6 col-sm-12 col-12 layout-spacing">
                            <div class="widget widget-table-one">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        <?php echo e(__('Team Members')); ?> (<?php echo e(count($project->users)); ?>)
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        <?php if($currantWorkspace->permission == 'Owner'): ?>
                                            <a href="#" class="btn btn-sm btn-primary" data-ajax-popup="true" data-title="<?php echo e(__('Invite')); ?>" data-url="<?php echo e(route('projects.invite.popup',[$currantWorkspace->slug,$project->id])); ?>"><?php echo e(__('Invite')); ?></a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                        
                                <div class="widget-content" style="margin-top: 15px;">
                                    <?php $__currentLoopData = $project->users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="transactions-list">
                                        <div class="t-item">
                                            <div class="t-company-name">
                                                <div class="t-icon">
                                                        <img class="mr-3 rounded-circle" width="50" <?php if($user->avatar): ?> src="<?php echo e(asset('/storage/avatars/'.$user->avatar)); ?>" <?php else: ?> avatar="<?php echo e($user->name); ?>" <?php endif; ?> alt="avatar">
                                                </div>
                                                <div class="t-name">
                                                    <h4><?php echo e($user->name); ?></h4>
                                                    <p class="meta-date"><?php echo e($user->email); ?></p>
                                                </div>
                        
                                            </div>
                                            <div class="t-rate rate-dec">
                                                <?php if(auth()->guard('web')->check()): ?>
                                                <?php if($currantWorkspace->permission == 'Owner' && $user->id != Auth::user()->id): ?>
                                                    <a href="#" class="float-right" onclick="(confirm('<?php echo e(__('Are you sure ?')); ?>')?document.getElementById('delete-user-<?php echo e($user->id); ?>').submit(): '');">
                                                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="#e7515a" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                    </a>
                                                    <form id="delete-user-<?php echo e($user->id); ?>" action="<?php echo e(route('projects.user.delete',[$currantWorkspace->slug,$project->id,$user->id])); ?>" method="POST" style="display: none;">
                                                        <?php echo csrf_field(); ?>
                                                        <?php echo method_field('DELETE'); ?>
                                                    </form>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-12 col-md-6 col-sm-12 col-12 layout-spacing">
                            <div class="widget widget-table-one">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        <?php echo e(__('Clients')); ?> (<?php echo e(count($project->clients)); ?>)
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        <?php if($currantWorkspace->permission == 'Owner'): ?>
                                                <a href="#" class="btn btn-sm btn-primary" data-ajax-popup="true" data-title="<?php echo e(__('Share to Clients')); ?>" data-url="<?php echo e(route('projects.share.popup',[$currantWorkspace->slug,$project->id])); ?>"><?php echo e(__('Share')); ?></a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                        
                                <div class="widget-content" style="margin-top: 15px;">
                                    <?php $__currentLoopData = $project->clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="transactions-list">
                                        <div class="t-item">
                                            <div class="t-company-name">
                                                <div class="t-icon">
                                                    <img class="mr-3 rounded-circle" width="50" <?php if($client->avatar): ?> src="<?php echo e(asset('/storage/avatars/'.$client->avatar)); ?>" <?php else: ?> avatar="<?php echo e($client->name); ?>" <?php endif; ?> alt="avatar">
                                                </div>
                                                <div class="t-name">
                                                    <h4><?php echo e($client->name); ?></h4>
                                                    <p class="meta-date"><?php echo e($client->email); ?></p>
                                                </div>
                        
                                            </div>
                                            <div class="t-rate rate-dec">
                                                <?php if(auth()->guard('web')->check()): ?>
                                                    <?php if($currantWorkspace->permission == 'Owner'): ?>
                                                        <a href="#" class="float-right" onclick="(confirm('<?php echo e(__('Are you sure ?')); ?>')?document.getElementById('delete-client-<?php echo e($client->id); ?>').submit(): '');">
                                                            <svg viewBox="0 0 24 24" width="24" height="24" stroke="#e7515a" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                        </a>
                                                        <a href="#" class="float-right mr-1" data-ajax-popup="true" data-size="lg" data-title="<?php echo e(__('Edit Permission')); ?>" data-url="<?php echo e(route('projects.client.permission',[$currantWorkspace->slug,$project->id,$client->id])); ?>">
                                                            <svg viewBox="0 0 24 24" width="24" height="24" stroke="#1b55e2" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>
                                                        </a>
                                                        <form id="delete-client-<?php echo e($client->id); ?>" action="<?php echo e(route('projects.client.delete',[$currantWorkspace->slug,$project->id,$client->id])); ?>" method="POST" style="display: none;">
                                                            <?php echo csrf_field(); ?>
                                                            <?php echo method_field('DELETE'); ?>
                                                        </form>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if((isset($permisions) && in_array('show milestone',$permisions)) || $currantWorkspace->permission == 'Owner'): ?>
                        <div class="layout-spacing">
                            <div class="widget widget-table-one">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        <?php echo e(__('Milestones')); ?> (<?php echo e(count($project->milestones)); ?>)
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        <?php if((isset($permisions) && in_array('create milestone',$permisions)) || $currantWorkspace->permission == 'Owner'): ?>
                                            <a href="#" class="btn btn-sm btn-primary" data-ajax-popup="true" data-title="<?php echo e(__('Create Milestone')); ?>" data-url="<?php if(auth()->guard('web')->check()): ?><?php echo e(route('projects.milestone',[$currantWorkspace->slug,$project->id])); ?><?php elseif(auth()->guard()->check()): ?><?php echo e(route('client.projects.milestone',[$currantWorkspace->slug,$project->id])); ?><?php endif; ?>"><?php echo e(__('Create Milestone')); ?></a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                        
                                <div class="widget-content" style="margin-top: 15px;">
                                    <?php $__currentLoopData = $project->milestones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $milestone): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="transactions-list">
                                        <div class="t-item">
                                            <div class="t-company-name">
                                                <div class="t-icon">
                                                    <div class="icon">
                                                        #<?php echo e($key+1); ?>

                                                    </div>
                                                </div>
                                                <div class="t-name">
                                                    <a href="#" class="milestone-detail" data-ajax-popup="true" data-title="<?php echo e(__('Milestones Details')); ?>" data-url="<?php if(auth()->guard('web')->check()): ?><?php echo e(route('projects.milestone.show',[$currantWorkspace->slug,$milestone->id])); ?><?php elseif(auth()->guard()->check()): ?><?php echo e(route('client.projects.milestone.show',[$currantWorkspace->slug,$milestone->id])); ?><?php endif; ?>"><?php echo e($milestone->title); ?></a>
                                                    <p class="meta-date">
                                                        <?php if($milestone->status == 'incomplete'): ?>
                                                            <label class="badge badge-warning"><?php echo e(__('Incomplete')); ?></label>
                                                        <?php endif; ?>
                                                        <?php if($milestone->status == 'complete'): ?>
                                                            <label class="badge badge-success"><?php echo e(__('Complete')); ?></label>
                                                        <?php endif; ?>
                                                    </p>
                                                </div>
                        
                                            </div>
                                            <div class="t-rate rate-dec">
                                                <p><?php echo e(__('Milestone Cost')); ?>: <span>$<?php echo e(number_format($milestone->cost)); ?></span></p>
                                            </div>
                                            <?php if($currantWorkspace->permission == 'Owner'): ?>
                                            <div class="float-right" style="margin-top: 12px;">
                                                    <a href="#" data-ajax-popup="true" data-title="<?php echo e(__('Edit Milestone')); ?>" data-url="<?php echo e(route('projects.milestone.edit',[$currantWorkspace->slug,$milestone->id])); ?>">
                                                        <svg viewBox="0 0 24 24" width="20" height="20" stroke="#1b55e2" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                                    </a>
                                                    <a href="#" onclick="(confirm('<?php echo e(__('Are you sure ?')); ?>')?document.getElementById('delete-form1-<?php echo e($milestone->id); ?>').submit(): '');">
                                                        <svg viewBox="0 0 24 24" width="20" height="20" stroke="#e7515a" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                    </a>
                                                    <form id="delete-form1-<?php echo e($milestone->id); ?>" action="<?php echo e(route('projects.milestone.destroy',[$currantWorkspace->slug,$milestone->id])); ?>" method="POST" style="display: none;">
                                                        <?php echo csrf_field(); ?>
                                                        <?php echo method_field('DELETE'); ?>
                                                    </form>
                                            </div>
                                        <?php elseif(isset($permisions)): ?>
                                        <div class="float-right">
                                                <?php if(in_array('edit milestone',$permisions)): ?>
                                                    <a href="#" class="btn btn-sm btn-outline-primary" data-ajax-popup="true" data-title="<?php echo e(__('Edit Milestone')); ?>" data-url="<?php echo e(route('client.projects.milestone.edit',[$currantWorkspace->slug,$milestone->id])); ?>"><i class="mdi mdi-pencil"></i></a>
                                                <?php endif; ?>
                                                <?php if(in_array('delete milestone',$permisions)): ?>
                                                    <a href="#" class="btn btn-sm btn-outline-danger" onclick="(confirm('<?php echo e(__('Are you sure ?')); ?>')?document.getElementById('delete-form1-<?php echo e($milestone->id); ?>').submit(): '');"><i class="mdi mdi-delete"></i></a>
                                                    <form id="delete-form1-<?php echo e($milestone->id); ?>" action="<?php echo e(route('client.projects.milestone.destroy',[$currantWorkspace->slug,$milestone->id])); ?>" method="POST" style="display: none;">
                                                        <?php echo csrf_field(); ?>
                                                        <?php echo method_field('DELETE'); ?>
                                                    </form>
                                                <?php endif; ?>
                                        </div>
                                        <?php endif; ?>
                                        </div>
                                    </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <!-- <?php if((isset($permisions) && in_array('show uploading',$permisions)) || $currantWorkspace->permission == 'Owner'): ?>
                        <div class="card author-box card-primary">
                            <div class="card-body">
                                <div class="author-box-name mb-4">
                                    <?php echo e(__('Files')); ?>

                                </div>
                                <div class="col-md-12 dropzone" id="dropzonewidget">
                                    <div class="dz-message" data-dz-message><span><?php echo e(__('Drop files here to upload')); ?></span></div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?> -->
                    <!-- end card-->
                </div> <!-- end col -->

                <div class="col-md-4 animated">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4><?php echo e(__('Progress')); ?></h4>
                        </div>
                        <div class="card-body">
                            <div style="height: 283px;">
                                <div id="revenueMonthly"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end card-->
                    <?php if((isset($permisions) && in_array('show activity',$permisions)) || $currantWorkspace->permission == 'Owner'): ?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4><?php echo e(__('Activity')); ?></h4>
                        </div>
                        <div class="card-body pr-2 pl-3">
                            <div class="activities top-10-scroll">
                                <?php $__currentLoopData = $project->activities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $activity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="activity">
                                        <?php if($activity->log_type == 'Move'): ?>
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-cursor-move"></i>
                                            </div>
                                        <?php elseif($activity->log_type == 'Create Milestone'): ?>
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-target"></i>
                                            </div>
                                        <?php elseif($activity->log_type == 'Create Task'): ?>
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-format-list-checks"></i>
                                            </div>
                                        <?php elseif($activity->log_type == 'Invite User'): ?>
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-plus"></i>
                                            </div>
                                        <?php elseif($activity->log_type == 'Share with Client'): ?>
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-plus"></i>
                                            </div>
                                        <?php elseif($activity->log_type == 'Upload File'): ?>
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-file"></i>
                                            </div>
                                        <?php elseif($activity->log_type == 'Create Timesheet'): ?>
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-clock"></i>
                                            </div>
                                        <?php elseif($activity->log_type == 'Create Bug'): ?>
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-bug"></i>
                                            </div>
                                        <?php elseif($activity->log_type == 'Move Bug'): ?>
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="mdi mdi-cursor-move"></i>
                                            </div>
                                        <?php endif; ?>
                                        <div class="activity-detail">
                                            <div class="mb-2">
                                                <span class="text-job"><?php echo e($activity->created_at->diffForHumans()); ?></span>
                                            </div>
                                            <p><?php echo $activity->getRemark(); ?></p>
                                        </div>

                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>
            </div>
        <?php else: ?>
            <div class="container mt-5">
                <div class="page-error">
                    <div class="page-inner">
                        <h1>404</h1>
                        <div class="page-description">
                            <?php echo e(__('Page Not Found')); ?>

                        </div>
                        <div class="page-search">
                            <p class="text-muted mt-3"><?php echo e(__('It\'s looking like you may have taken a wrong turn. Don\'t worry... it happens to the best of us. Here\'s a little tip that might help you get back on track.')); ?></p>
                            <div class="mt-3">
                                <a class="btn btn-info mt-3" href="<?php echo e(route('home')); ?>"><i class="mdi mdi-reply"></i> <?php echo e(__('Return Home')); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>


    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('style'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/dropzone.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('src/css/dashboard/dash_1.css')); ?>">
<?php $__env->stopPush(); ?>
<?php $__env->startPush('scripts'); ?>
    <!-- third party js -->
    <script src="<?php echo e(asset('plugins/apex/apexcharts.min.js')); ?>"></script>
    <script>

var options1 = {
  chart: {
    fontFamily: 'Nunito, sans-serif',
    height: 270,
    type: 'area',
    zoom: {
        enabled: false
    },
    dropShadow: {
      enabled: true,
      opacity: 0.3,
      blur: 5,
      left: -7,
      top: 22
    },
    toolbar: {
      show: false
    },
    events: {
      mounted: function(ctx, config) {
        const highest1 = ctx.getHighestValueInSeries(0);
        const highest2 = ctx.getHighestValueInSeries(1);

        ctx.addPointAnnotation({
          x: new Date(ctx.w.globals.seriesX[0][ctx.w.globals.series[0].indexOf(highest1)]).getTime(),
          y: highest1,
          label: {
            style: {
              cssClass: 'd-none'
            }
          },
          customSVG: {
              SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#1b55e2" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
              cssClass: undefined,
              offsetX: -8,
              offsetY: 5
          }
        })

        ctx.addPointAnnotation({
          x: new Date(ctx.w.globals.seriesX[1][ctx.w.globals.series[1].indexOf(highest2)]).getTime(),
          y: highest2,
          label: {
            style: {
              cssClass: 'd-none'
            }
          },
          customSVG: {
              SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#e7515a" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
              cssClass: undefined,
              offsetX: -8,
              offsetY: 5
          }
        })
      },
    }
  },
  colors: ['#1b55e2', '#e7515a', '#0acf97', '#727cf5', '#875767'],
  dataLabels: {
      enabled: false
  },
  markers: {
    discrete: [{
    seriesIndex: 0,
    dataPointIndex: 7,
    fillColor: '#000',
    strokeColor: '#000',
    size: 5
  }, {
    seriesIndex: 2,
    dataPointIndex: 11,
    fillColor: '#000',
    strokeColor: '#000',
    size: 4
  }]
  },
  subtitle: {
    // text: 'Task Overview',
    align: 'left',
    margin: 0,
    offsetX: -10,
    offsetY: 35,
    floating: false,
    style: {
      fontSize: '14px',
      color:  '#888ea8'
    }
  },
  title: {
    align: 'left',
    margin: 0,
    offsetX: -10,
    offsetY: 0,
    floating: false,
    style: {
      fontSize: '25px',
      color:  '#bfc9d4'
    },
  },
  stroke: {
      show: true,
      curve: 'smooth',
      width: 2,
      lineCap: 'square'
  },
  series: [{
      name: "<?php echo e(__('Todo')); ?>",
      data: <?php echo json_encode($chartData['todo']); ?>

  }, {
      name: "<?php echo e(__('In Progress')); ?>",
      data: <?php echo json_encode($chartData['progress']); ?>

  }, {
      name: "<?php echo e(__('Review')); ?>",
      data: <?php echo json_encode($chartData['review']); ?>

  }, {
      name: "<?php echo e(__('Done')); ?>",
      data: <?php echo json_encode($chartData['done']); ?>

  }],
  labels: <?php echo json_encode($chartData['label']); ?>,
  xaxis: {
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    },
    crosshairs: {
      show: true
    },
    labels: {
      offsetX: 0,
      offsetY: 5,
      style: {
          fontSize: '12px',
          fontFamily: 'Nunito, sans-serif',
          cssClass: 'apexcharts-xaxis-title',
      },
    }
  },
  yaxis: {
    labels: {
      formatter: function(value, index) {
        return (value)
      },
      offsetX: -22,
      offsetY: 0,
      style: {
          fontSize: '12px',
          fontFamily: 'Nunito, sans-serif',
          cssClass: 'apexcharts-yaxis-title',
      },
    }
  },
  grid: {
    borderColor: '#191e3a',
    strokeDashArray: 5,
    xaxis: {
        lines: {
            show: true
        }
    },   
    yaxis: {
        lines: {
            show: false,
        }
    },
    padding: {
      top: 0,
      right: 0,
      bottom: 0,
      left: -10
    }, 
  }, 
  legend: {
    position: 'top',
    horizontalAlign: 'right',
    offsetY: -50,
    fontSize: '16px',
    fontFamily: 'Nunito, sans-serif',
    markers: {
      width: 10,
      height: 10,
      strokeWidth: 0,
      strokeColor: '#fff',
      fillColors: undefined,
      radius: 12,
      onClick: undefined,
      offsetX: 0,
      offsetY: 0
    },    
    itemMargin: {
      horizontal: 0,
      vertical: 20
    }
  },
  tooltip: {
    theme: 'dark',
    marker: {
      show: true,
    },
    x: {
      show: false,
    }
  },
  fill: {
      type:"gradient",
      gradient: {
          type: "vertical",
          shadeIntensity: 1,
          inverseColors: !1,
          opacityFrom: .28,
          opacityTo: .05,
          stops: [45, 100]
      }
  },
  responsive: [{
    breakpoint: 575,
    options: {
      legend: {
          offsetY: -30,
      },
    },
  }]
}

var chart1 = new ApexCharts(
    document.querySelector("#revenueMonthly"),
    options1
);

chart1.render();


    </script>
    <!-- third party js ends -->

    <script src="<?php echo e(asset('assets/js/dropzone.min.js')); ?>"></script>
    <script>
        Dropzone.autoDiscover = false;
        myDropzone = new Dropzone("#dropzonewidget", {
            maxFiles: 20,
            maxFilesize: 2,
            parallelUploads: 1,
            acceptedFiles: ".jpeg,.jpg,.png,.pdf,.doc,.txt",
            url: "<?php echo e(route('projects.file.upload',[$currantWorkspace->slug,$project->id])); ?>",
            success: function (file, response) {
                if (response.is_success) {
                    dropzoneBtn(file, response);
                } else {
                    myDropzone.removeFile(file);
                    toastr('<?php echo e(__('Error')); ?>', response.error, 'error');
                }
            },
            error: function (file, response) {
                myDropzone.removeFile(file);
                if (response.error) {
                    toastr('<?php echo e(__('Error')); ?>', response.error, 'error');
                } else {
                    toastr('<?php echo e(__('Error')); ?>', response, 'error');
                }
            }
        });
        myDropzone.on("sending", function (file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
            formData.append("project_id", <?php echo e($project->id); ?>);
        });

        <?php if(isset($permisions) && in_array('show uploading',$permisions)): ?>
            $(".dz-hidden-input").prop("disabled",true);
            myDropzone.removeEventListeners();
        <?php endif; ?>

        function dropzoneBtn(file, response) {

            var html = document.createElement('div');

            var download = document.createElement('a');
            download.setAttribute('href', response.download);
            download.setAttribute('class', "btn btn btn-outline-primary btn-sm mt-1 mr-1");
            download.setAttribute('data-toggle', "tooltip");
            download.setAttribute('data-original-title', "<?php echo e(__('Download')); ?>");
            download.innerHTML = "<i class='mdi mdi-download'></i>";
            html.appendChild(download);

            <?php if(isset($permisions) && in_array('show uploading',$permisions)): ?>
            <?php else: ?>
                var del = document.createElement('a');
                del.setAttribute('href', response.delete);
                del.setAttribute('class', "btn btn-outline-danger btn-sm mt-1");
                del.setAttribute('data-toggle', "tooltip");
                del.setAttribute('data-original-title', "<?php echo e(__('Delete')); ?>");
                del.innerHTML = "<i class='mdi mdi-delete'></i>";

                del.addEventListener("click", function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (confirm("Are you sure ?")) {
                        var btn = $(this);
                        $.ajax({
                            url: btn.attr('href'),
                            data: {_token: $('meta[name="csrf-token"]').attr('content')},
                            type: 'DELETE',
                            success: function (response) {
                                if (response.is_success) {
                                    btn.closest('.dz-image-preview').remove();
                                } else {
                                    toastr('<?php echo e(__('Error')); ?>', response.error, 'error');
                                }
                            },
                            error: function (response) {
                                response = response.responseJSON;
                                if (response.is_success) {
                                    toastr('<?php echo e(__('Error')); ?>', response.error, 'error');
                                } else {
                                    toastr('<?php echo e(__('Error')); ?>', response, 'error');
                                }
                            }
                        })
                    }
                });
                html.appendChild(del);
            <?php endif; ?>





            file.previewTemplate.appendChild(html);
        }

        <?php
            $files = $project->files;
        ?>
        <?php $__currentLoopData = $files; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        // Create the mock file:
        var mockFile = {name: "<?php echo e($file->file_name); ?>", size: <?php echo e(filesize(storage_path('project_files/'.$file->file_path))); ?> };
        // Call the default addedfile event handler
        myDropzone.emit("addedfile", mockFile);
        // And optionally show the thumbnail of the file:
        myDropzone.emit("thumbnail", mockFile, "<?php echo e(asset('storage/project_files/'.$file->file_path)); ?>");
        myDropzone.emit("complete", mockFile);

        dropzoneBtn(mockFile, {download: "<?php if(auth()->guard('web')->check()): ?><?php echo e(route('projects.file.download',[$currantWorkspace->slug,$project->id,$file->id])); ?><?php elseif(auth()->guard()->check()): ?><?php echo e(route('client.projects.file.download',[$currantWorkspace->slug,$project->id,$file->id])); ?><?php endif; ?>", delete: "<?php echo e(route('projects.file.delete',[$currantWorkspace->slug,$project->id,$file->id])); ?>"});

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/kevindupas/Documents/Dev/Tinker/resources/views/projects/show.blade.php ENDPATH**/ ?>