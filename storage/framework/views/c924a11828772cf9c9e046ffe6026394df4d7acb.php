<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Login | Register </title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <!-- App css -->
    <link href="<?php echo e(asset('bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('src/css/plugins.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('src/css/authentication/form-2.css')); ?>" rel="stylesheet">
</head>
<body class="form">
    

    <div class="form-container outer">
        <div class="form-form">
            <div class="form-form-wrap">
                <div class="form-container">
                    <div class="form-content">

                        <?php if(session()->has('info')): ?>
                        <div class="alert alert-primary">
                            <?php echo e(session()->get('info')); ?>

                        </div>
                        <?php endif; ?>
                        <?php if(session()->has('status')): ?>
                            <div class="alert alert-info">
                                <?php echo e(session()->get('status')); ?>

                            </div>
                        <?php endif; ?>
                        <!-- Contenu -->
                        <?php echo $__env->yieldContent('content'); ?>
                        <!-- Fin contenu -->

                    </div>                    
                </div>
            </div>
        </div>
    </div>

    
    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?php echo e(asset('src/js/libs/jquery-3.1.1.min.js')); ?>"></script>
    <script src="<?php echo e(asset('bootstrap/js/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('src/js/authentication/form-2.js')); ?>"></script>

</body>
</html><?php /**PATH /Users/kevindupas/Documents/Dev/Tinker/resources/views/layouts/auth2.blade.php ENDPATH**/ ?>