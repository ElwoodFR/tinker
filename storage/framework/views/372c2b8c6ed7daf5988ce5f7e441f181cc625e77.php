<?php $__env->startSection('content'); ?>

<div class="row layout-top-spacing layout-spacing">
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">

            <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                    <table id="style-3" class="table style-3  table-hover">
                        <thead>
                            <tr>
                                <th class=""> <?php echo e(__('Task')); ?> </th>
                                <th class=""><?php echo e(__('Project')); ?></th>
                                <th><?php echo e(__('Milestone')); ?></th>
                                <th><?php echo e(__('Due in')); ?></th>
                                <?php if($currantWorkspace->permission == 'Owner' || Auth::user()->getGuard() == 'client'): ?>
                                <th><?php echo e(__('Assigned to')); ?></th>
                                <?php endif; ?>
                                <th><?php echo e(__('Status')); ?></th>
                                <th><?php echo e(__('Priority')); ?></th>
                                <?php if($currantWorkspace->permission == 'Owner'): ?>
                                <th class="text-center"><?php echo e(__('Action')); ?></th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td>
                                    <a href="<?php echo e(route('projects.task.board',[$currantWorkspace->slug,$task->project_id])); ?>"><?php echo e($task->title); ?></a>
                                </td>
                                <td>
                                    <?php echo e($task->project->name); ?>

                                </td>
                                <td><?php if($milestone = $task->milestone()): ?><?php echo e($milestone->title); ?><?php endif; ?></td>
                                <td>
                                    <?php echo e(\App\Utility::get_timeago(strtotime($task->due_date))); ?>

                                </td>
                                <?php if($currantWorkspace->permission == 'Owner' || Auth::user()->getGuard() == 'client'): ?>
                                <td>
                                    <img <?php if($task->user->avatar): ?> src="<?php echo e(asset('/storage/avatars/'.$task->user->avatar)); ?>" class="profile-img" alt="avatar" <?php else: ?> avatar="<?php echo e($task->user->name); ?>"<?php endif; ?> class="rounded-circle">
                                </td>
                                <?php endif; ?>
                                <td>
                                    <?php if($task->status=='todo'): ?>
                                        <span class="badge outline-badge-primary"><?php echo e(__(ucwords($task->status))); ?></span>
                                    <?php elseif($task->status=='in progress'): ?>
                                        <span class="badge outline-badge-warning"><?php echo e(__(ucwords($task->status))); ?></span>
                                    <?php elseif($task->status=='review'): ?>
                                        <span class="badge outline-badge-danger"><?php echo e(__(ucwords($task->status))); ?></span>
                                    <?php elseif($task->status=='done'): ?>
                                        <span class="badge outline-badge-success"><?php echo e(__(ucwords($task->status))); ?></span>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if($task->priority=="High"): ?>
                                        <span class="badge outline-badge-danger"><?php echo e(__('High')); ?></span>
                                    <?php elseif($task->priority=="Medium"): ?>
                                        <span class="badge outline-badge-warning"><?php echo e(__('Medium')); ?></span>
                                    <?php else: ?>
                                        <span class="badge outline-badge-success"><?php echo e(__('Low')); ?></span>
                                    <?php endif; ?>
                                </td>
                                <?php if($currantWorkspace->permission == 'Owner'): ?>
                                <td class="text-center">
                                    <ul class="table-controls">
                                        <a href="#" class="bs-tooltip" data-ajax-popup="true" data-size="lg" data-title="<?php echo e(__('Edit Task')); ?>" data-url="<?php echo e(route('tasks.show',[$currantWorkspace->slug,$task->project_id,$task->id])); ?>">
                                            <svg viewBox="0 0 24 24" width="20" height="20" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg><a href="#" class="bs-tooltip" data-ajax-popup="true" data-size="lg" data-title="<?php echo e(__('Edit Task')); ?>" data-url="<?php echo e(route('tasks.edit',[$currantWorkspace->slug,$task->project_id,$task->id])); ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 p-1 br-6 mb-1"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>     
                                        </a>
                                        <a href="#" class="bs-tooltip" onclick="(confirm('<?php echo e(__('Are you sure ?')); ?>')?document.getElementById('delete-form-<?php echo e($task->id); ?>').submit(): '');">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash p-1 br-6 mb-1"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
                                        </a>
                                        <form id="delete-form-<?php echo e($task->id); ?>" action="<?php echo e(route('tasks.destroy',[$currantWorkspace->slug,$task->project_id,$task->id])); ?>" method="POST" style="display: none;">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('DELETE'); ?>
                                        </form>
                                    </ul>
                                </td>
                                <?php endif; ?>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('style'); ?>
<link href="<?php echo e(asset('plugins/table/datatable/datatables.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('src/css/forms/theme-checkbox-radio.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('plugins/table/datatable/dt-global_style.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('plugins/table/datatable/custom_dt_custom.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
    <script src="<?php echo e(asset('plugins/table/datatable/datatables.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/vendor/dataTables.responsive.min.js')); ?>"></script>
    <script>

        c3 = $('#style-3').DataTable({
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
               "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [5, 10, 20, 50],
            "pageLength": 5
        });

        multiCheck(c3);
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/kevindupas/Documents/Dev/Tinker/resources/views/projects/tasks.blade.php ENDPATH**/ ?>