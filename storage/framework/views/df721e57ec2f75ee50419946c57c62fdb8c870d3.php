<?php $__env->startSection('content'); ?>

<div class="row layout-spacing layout-top-spacing" id="cancel-row">
    <?php if($currantWorkspace): ?>
    <div class="col-lg-12">
        <div class="widget-content searchable-container grid">

            <div class="row">
                <div class="col-xl-4 col-lg-5 col-md-5 col-sm-7 filtered-list-search layout-spacing align-self-center">
                    <form class="form-inline my-2 my-lg-0">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-search">
                                <circle cx="11" cy="11" r="8"></circle>
                                <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                            </svg>
                            <input type="text" class="form-control product-search" id="input-search"
                                placeholder="Search Contacts...">
                        </div>
                    </form>
                </div>

                <div
                    class="col-xl-8 col-lg-7 col-md-7 col-sm-5 text-sm-right text-center layout-spacing align-self-center">
                    <div class="d-flex justify-content-sm-end justify-content-center">

                        <?php if($currantWorkspace && $currantWorkspace->creater->id == Auth::user()->id): ?>
                        <a id="btn-add-contact" data-ajax-popup="true" data-size="lg"
                            data-title="<?php echo e(__('Invite New User')); ?>"
                            data-url="<?php echo e(route('users.invite',$currantWorkspace->slug)); ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-user-plus">
                                <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                <circle cx="8.5" cy="7" r="4"></circle>
                                <line x1="20" y1="8" x2="20" y2="14"></line>
                                <line x1="23" y1="11" x2="17" y2="11"></line>
                            </svg>
                        </a>
                        <?php endif; ?>
                    </div>

                </div>
            </div>

            <div class="searchable-items grid">
                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="items">
                    <div class="item-content">
                        <?php if($currantWorkspace && $currantWorkspace->permission == 'Owner' && Auth::user()->id != $user->id): ?>
                        <div class="action-btn text-right">
                            <a data-ajax-popup="true" data-title="<?php echo e(__('Edit User')); ?>"
                            data-url="<?php echo e(route('users.edit',[$currantWorkspace->slug,$user->id])); ?>">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-edit-2 edit">
                                    <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                </svg>
                            </a>
                            <a onclick="(confirm('<?php echo e(__('Are you sure ?')); ?>')?document.getElementById('delete-form-<?php echo e($user->id); ?>').submit(): '');">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-user-minus delete">
                                <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                <circle cx="8.5" cy="7" r="4"></circle>
                                <line x1="23" y1="11" x2="17" y2="11"></line>
                            </svg>
                            </a>
                            <form method="post" id="delete-form-<?php echo e($user->id); ?>"
                                action="<?php echo e(route('users.remove',[$currantWorkspace->slug,$user->id])); ?>">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('DELETE'); ?>
                            </form>
                        </div>
                        <?php endif; ?>
                        <div class="user-profile">
                            <img <?php if($user->avatar): ?> src="<?php echo e(asset('/storage/avatars/'.$user->avatar)); ?>" width="75px"
                            <?php else: ?> avatar="<?php echo e($user->name); ?>" <?php endif; ?> alt="" class="rounded-circle
                            profile-widget-picture" width="75px">
                            <div class="user-meta-info">
                                <p class="user-name" data-name="<?php echo e($user->name); ?>"><?php echo e($user->name); ?> / <span><?php echo e(__($user->permission)); ?></span></p>
                                <p class="user-work" data-occupation="Web Developer">Web Developer</p>
                            </div>
                        </div>
                        <div class="user-email">
                            <p class="info-title"><?php echo e(__('Email Address')); ?> :</p>
                            <p class="usr-email-addr">
                                <?php echo e($user->email); ?>

                            </p>
                        </div>
                        <div class="user-location">
                            <p class="info-title"><?php echo e(__('Number of Projects')); ?> :</p>
                            <p class="usr-location">
                                <?php echo e($user->countProject(($currantWorkspace)?$currantWorkspace->id:'')); ?>

                            </p>
                        </div>
                        <div class="user-location">
                            <p class="info-title"><?php echo e(__('Number of Tasks')); ?> :</p>
                            <p class="usr-location">
                                <?php echo e($user->countTask(($currantWorkspace)?$currantWorkspace->id:'')); ?>

                            </p>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

        </div>
    </div>
    <?php else: ?>
    <div class="container mt-5">
        <div class="page-error">
            <div class="page-inner">
                <h1>404</h1>
                <div class="page-description">
                    <?php echo e(__('Page Not Found')); ?>

                </div>
                <div class="page-search">
                    <p class="text-muted mt-3">
                        <?php echo e(__('It\'s looking like you may have taken a wrong turn. Don\'t worry... it happens to the best of us. Here\'s a little tip that might help you get back on track.')); ?>

                    </p>
                    <div class="mt-3">
                        <a class="btn btn-info mt-3" href="<?php echo e(route('home')); ?>"><i class="mdi mdi-reply"></i>
                            <?php echo e(__('Return Home')); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>

<?php $__env->stopSection(); ?>

<?php if($currantWorkspace): ?>
<?php $__env->startPush('style'); ?>
<link rel="stylesheet" href="<?php echo e(asset('src/css/forms/theme-checkbox-radio.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/jquery-ui/jquery-ui.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('src/css/apps/contacts.css')); ?>">
<?php $__env->stopPush(); ?>


<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('src/js/custom.js')); ?>"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo e(asset('plugins/jquery-ui/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('src/js/apps/contact.js')); ?>"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<?php $__env->stopPush(); ?>
<?php endif; ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/kevindupas/Documents/Dev/Tinker/resources/views/users/index.blade.php ENDPATH**/ ?>