<li class="menu">
    <a href="<?php echo e(route('home')); ?>" data-active="<?php echo e((Request::route()->getName() == 'home' || Request::route()->getName() == NULL) ? 'true' : 'false'); ?>" aria-expanded="false" class="dropdown-toggle">
        <div class="">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg>
            <span><?php echo e(__('Dashboard')); ?></span>
        </div>
    </a>
</li>

<?php if(isset($currantWorkspace) && $currantWorkspace): ?>
    <li class="menu">
        <a href="<?php echo e(route('projects.index',$currantWorkspace->slug)); ?>" data-active="<?php echo e((Request::route()->getName() == 'projects.index') ? 'true' : 'false'); ?>" aria-expanded="false" class="dropdown-toggle">
            <div class="">
                <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect><path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path></svg>
                <span><?php echo e(__('Projects')); ?></span>
            </div>
        </a>
    </li>
    <?php if(auth()->guard('web')->check()): ?>
        <li class="menu">
            <a href="<?php echo e(route('tasks.index',$currantWorkspace->slug)); ?>" data-active="<?php echo e((Request::route()->getName() == 'tasks.index') ? 'true' : 'false'); ?>" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                    <span><?php echo e(__('Tasks')); ?></span>
                </div>
            </a>
        </li>
        <li class="menu" data-active="<?php echo e((Request::route()->getName() == 'users.index') ? 'true' : 'false'); ?>">
            <a href="<?php echo e(route('users.index',$currantWorkspace->slug)); ?>" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                    <span><?php echo e(__('Users')); ?></span>
                </div>
            </a>
        </li>
        <?php if($currantWorkspace->creater->id == Auth::user()->id): ?>
        <li class="menu">
            <a href="<?php echo e(route('clients.index',$currantWorkspace->slug)); ?>" data-active="<?php echo e((Request::route()->getName() == 'clients.index') ? 'true' : 'false'); ?>" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                    <span><?php echo e(__('Clients')); ?></span>
                </div>
            </a>
        </li>
        <?php endif; ?>
        <li class="menu">
            <a href="<?php echo e(route('calender.index',$currantWorkspace->slug)); ?>" data-active="<?php echo e((Request::route()->getName() == 'calender.index') ? 'true' : 'false'); ?>" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                    <span><?php echo e(__('Calendar')); ?></span>
                </div>
            </a>
        </li>
        <li class="menu">
            <a href="<?php echo e(route('notes.index',$currantWorkspace->slug)); ?>" data-active="<?php echo e((Request::route()->getName() == 'notes.index') ? 'true' : 'false'); ?>" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                    <span><?php echo e(__('Notes')); ?></span>
                </div>
            </a>
        </li>
        <?php if(env('CHAT_MODULE') == 'yes'): ?>
        <li class="menu">
            <a href="<?php echo e(route('chats.index',$currantWorkspace->slug)); ?>" data-active="<?php echo e((Request::route()->getName() == 'chats.index') ? 'true' : 'false'); ?>" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                    <span><?php echo e(__('Chat')); ?></span>
                </div>
            </a>
        </li>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?><?php /**PATH /Users/kevindupas/Documents/Dev/Tinker/resources/views/partials/sidebar.blade.php ENDPATH**/ ?>