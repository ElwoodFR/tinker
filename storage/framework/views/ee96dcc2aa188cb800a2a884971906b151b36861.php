<?php $__env->startSection('content'); ?>

<div class="row app-notes layout-top-spacing" id="cancel-row">
    <div class="col-lg-12">
        <div class="app-hamburger-container">
            <div class="hamburger"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                    stroke-linecap="round" stroke-linejoin="round"
                    class="feather feather-menu chat-menu d-xl-none">
                    <line x1="3" y1="12" x2="21" y2="12"></line>
                    <line x1="3" y1="6" x2="21" y2="6"></line>
                    <line x1="3" y1="18" x2="21" y2="18"></line>
                </svg></div>
        </div>

        <div class="app-container">

            <div class="app-note-container">

                <div class="app-note-overlay"></div>

                <div class="tab-title">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12 text-center">
                            <button type="button" id="btn-add-notes" class="btn btn-primary" data-ajax-popup="true" data-size="lg" data-title="<?php echo e(__('Create New Note')); ?>" data-url="<?php echo e(route('notes.create',$currantWorkspace->slug)); ?>">
                                <?php echo e(__('Create Note')); ?>

                            </button>
                        </div>
                        <div class="col-md-12 col-sm-12 col-12 mt-5">
                            <ul class="nav nav-pills d-block" id="pills-tab3" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link list-actions active" id="all-notes"><svg
                                            xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-edit">
                                            <path
                                                d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7">
                                            </path>
                                            <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z">
                                            </path>
                                        </svg> All Notes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link list-actions" id="note-fav"><svg
                                            xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-star">
                                            <polygon
                                                points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2">
                                            </polygon>
                                        </svg> Favourites</a>
                                </li>
                            </ul>

                            <hr />

                            <p class="group-section"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                    height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                    stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                    class="feather feather-tag">
                                    <path
                                        d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z">
                                    </path>
                                    <line x1="7" y1="7" x2="7" y2="7"></line>
                                </svg> Tags</p>

                            <ul class="nav nav-pills d-block group-list" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link list-actions g-dot-primary"
                                        id="note-personal">Personal</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link list-actions g-dot-warning" id="note-work">Work</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link list-actions g-dot-success" id="note-social">Social</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link list-actions g-dot-danger"
                                        id="note-important">Important</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <?php if(count($notes)): ?>
                <div id="ct" class="note-container note-grid">
                    <?php $__currentLoopData = $notes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $note): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="note-item all-notes <?php echo e($note->color); ?>">
                        <div class="note-inner-content">
                            <div class="note-content">
                                <p class="note-title" data-noteTitle="<?php echo e($note->title); ?>"><?php echo e($note->title); ?></p>
                                <p class="meta-time"><?php echo e($note->created_at); ?></p>
                                <div class="note-description-content">
                                    <p class="note-description" data-noteDescription="<?php echo e($note->text); ?>">
                                        <?php echo e($note->text); ?>

                                    </p>
                                </div>
                            </div>
                            <div class="note-action">
                                <a href="#" data-ajax-popup="true" data-size="lg" data-title="<?php echo e(__('Edit Note')); ?>" data-url="<?php echo e(route('notes.edit',[$currantWorkspace->slug,$note->id])); ?>"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit">
                                    <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7">
                                    </path>
                                    <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z">
                                    </path>
                                </svg></a>
                                <a href="#" onclick="(confirm('Are you sure ?')?document.getElementById('delete-form-<?php echo e($note->id); ?>').submit(): '');"><svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a>
                                <form id="delete-form-<?php echo e($note->id); ?>" action="<?php echo e(route('notes.destroy',[$currantWorkspace->slug,$note->id])); ?>" method="POST" style="display: none;">
                                    <?php echo csrf_field(); ?>
                                    <?php echo method_field('DELETE'); ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
                <?php endif; ?>

            </div>

        </div>

    </div>
</div>

<?php $__env->stopSection(); ?>

<?php if($currantWorkspace): ?>
<?php $__env->startPush('style'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('src/css/apps/notes.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('src/css/forms/theme-checkbox-radio.css')); ?>">
<?php $__env->stopPush(); ?>


<?php $__env->startPush('scripts'); ?>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo e(asset('src/js/ie11fix/fn.fix-padStart.js')); ?>"></script>
<script src="<?php echo e(asset('src/js/apps/notes.js')); ?>"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<?php $__env->stopPush(); ?>
<?php endif; ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/kevindupas/Documents/Dev/projectTinker/resources/views/notes/index.blade.php ENDPATH**/ ?>