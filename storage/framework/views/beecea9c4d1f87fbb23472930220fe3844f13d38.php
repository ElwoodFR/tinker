<?php (\App::setLocale( basename(App::getLocale()))); ?>
<div class="message-wrapper chat-content rounded">
    <?php if(count($messages) > 0): ?>
        <ul class="messages pl-1">
            <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="message clearfix">
                    <div class="<?php echo e(($message->from == Auth::id()) ? 'sent' : 'received'); ?>">
                        <p><?php echo e($message->message); ?></p>
                        <p class="date"><?php echo e($message->created_at->diffForHumans()); ?></p>
                    </div>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    <?php else: ?>
        <h3 class="text-center mt-5 pt-5"><?php echo e(__('No Message Found.!')); ?></h3>
    <?php endif; ?>
</div>
<div class="card chat-box">
    <div class="card-footer chat-form">
        <input type="text" class="submit form-control" name="message"  placeholder="Type a message">
        <button class="btn btn-primary">
            <i class="fa fa-paper-plane"></i>
        </button>
    </div>
</div><?php /**PATH /Users/kevindupas/Documents/Dev/Tinker/resources/views/chats/message.blade.php ENDPATH**/ ?>