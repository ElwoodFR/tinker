
<?php if($project && $currantWorkspace): ?>

    <form class="pl-3 pr-3" method="post" action="<?php if(auth()->guard('web')->check()): ?><?php echo e(route('tasks.store',[$currantWorkspace->slug,$project->id])); ?><?php elseif(auth()->guard()->check()): ?><?php echo e(route('client.tasks.store',[$currantWorkspace->slug,$project->id])); ?><?php endif; ?>">
        <?php echo csrf_field(); ?>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label><?php echo e(__('Project')); ?></label>
                    <select class="form-control form-control-light" name="project_id" required>
                        <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($p->id); ?>" <?php if($p->id == $project->id): ?> selected <?php endif; ?>><?php echo e($p->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="task-milestone"><?php echo e(__('Milestone')); ?></label>
                    <select class="form-control form-control-light" name="milestone_id" id="task-milestone">
                        <option value=""><?php echo e(__('Select Milestone')); ?></option>
                        <?php $__currentLoopData = $project->milestones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $milestone): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($milestone->id); ?>"><?php echo e($milestone->title); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="task-title"><?php echo e(__('Title')); ?></label>
                    <input type="text" class="form-control form-control-light" id="task-title"
                           placeholder="<?php echo e(__('Enter Title')); ?>" name="title" required>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="task-priority"><?php echo e(__('Priority')); ?></label>
                    <select class="form-control form-control-light" name="priority" id="task-priority" required>
                        <option value="Low"><?php echo e(__('Low')); ?></option>
                        <option value="Medium"><?php echo e(__('Medium')); ?></option>
                        <option value="High"><?php echo e(__('High')); ?></option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="assign_to"><?php echo e(__('Assign To')); ?></label>
                    <select class="form-control form-control-light" id="assign_to" name="assign_to" required>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $u): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($u->id); ?>"><?php echo e($u->name); ?> - <?php echo e($u->email); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="duration"><?php echo e(__('Duration')); ?></label>
                    <input type="text" class="form-control form-control-light" id="duration" name="duration" required autocomplete="off">
                    <input type="hidden" name="start_date">
                    <input type="hidden" name="due_date">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="task-description"><?php echo e(__('Description')); ?></label>
            <textarea class="form-control form-control-light" id="task-description" rows="3" name="description"></textarea>
        </div>

        <div class="text-right">
            <button type="button" class="btn btn-light" data-dismiss="modal"><?php echo e(__('Cancel')); ?></button>
            <button type="submit" class="btn btn-primary"><?php echo e(__('Create')); ?></button>
        </div>

    </form>
    <script>
        $(function() {
            var start = moment().startOf('hour');
            var end = moment().startOf('hour').add(32, 'hour');
            function cb(start, end) {
                $('input[name="start_date"]').val(start.format('YYYY-MM-DD HH:mm:ss'));
                $('input[name="due_date"]').val(end.format('YYYY-MM-DD HH:mm:ss'));
            }
            $('#duration').daterangepicker({
                timePicker: true,
                startDate: start,
                endDate: end,
                locale: {
                    format: 'MMMM D, YYYY hh:mm A',
                    applyLabel: "<?php echo e(__('Apply')); ?>",
                    cancelLabel: "<?php echo e(__('Cancel')); ?>",
                    fromLabel: "<?php echo e(__('From')); ?>",
                    toLabel: "<?php echo e(__('To')); ?>",
                    daysOfWeek: [
                        "<?php echo e(__('Sun')); ?>",
                        "<?php echo e(__('Mon')); ?>",
                        "<?php echo e(__('Tue')); ?>",
                        "<?php echo e(__('Wed')); ?>",
                        "<?php echo e(__('Thu')); ?>",
                        "<?php echo e(__('Fri')); ?>",
                        "<?php echo e(__('Sat')); ?>"
                    ],
                    monthNames: [
                        "<?php echo e(__('January')); ?>",
                        "<?php echo e(__('February')); ?>",
                        "<?php echo e(__('March')); ?>",
                        "<?php echo e(__('April')); ?>",
                        "<?php echo e(__('May')); ?>",
                        "<?php echo e(__('June')); ?>",
                        "<?php echo e(__('July')); ?>",
                        "<?php echo e(__('August')); ?>",
                        "<?php echo e(__('September')); ?>",
                        "<?php echo e(__('October')); ?>",
                        "<?php echo e(__('November')); ?>",
                        "<?php echo e(__('December')); ?>"
                    ],
                }
            },cb);
            cb(start,end);
        });
    </script>
    <script>
        $(document).on('change',"select[name=project_id]",function () {
            $.get('<?php if(auth()->guard('web')->check()): ?><?php echo e(route('home')); ?><?php elseif(auth()->guard()->check()): ?><?php echo e(route('client.home')); ?><?php endif; ?>'+'/userProjectJson/'+$(this).val(),function (data) {
                $('select[name=assign_to]').html('');
                data = JSON.parse(data);
                $(data).each(function(i,d){
                    $('select[name=assign_to]').append('<option value="'+d.id+'">'+d.name+' - '+d.email+'</option>');
                });
            });
            $.get('<?php if(auth()->guard('web')->check()): ?><?php echo e(route('home')); ?><?php elseif(auth()->guard()->check()): ?><?php echo e(route('client.home')); ?><?php endif; ?>'+'/projectMilestoneJson/'+$(this).val(),function (data) {
                $('select[name=milestone_id]').html('<option value=""><?php echo e(__('Select Milestone')); ?></option>');
                data = JSON.parse(data);
                $(data).each(function(i,d){
                    $('select[name=milestone_id]').append('<option value="'+d.id+'">'+d.title+'</option>');
                });
            })
        })
    </script>

<?php else: ?>
    <div class="container mt-5">
        <div class="page-error">
            <div class="page-inner">
                <h1>404</h1>
                <div class="page-description">
                    <?php echo e(__('Page Not Found')); ?>

                </div>
                <div class="page-search">
                    <p class="text-muted mt-3"><?php echo e(__('It\'s looking like you may have taken a wrong turn. Don\'t worry... it happens to the best of us. Here\'s a little tip that might help you get back on track.')); ?></p>
                    <div class="mt-3">
                        <a class="btn btn-info mt-3" href="<?php echo e(route('home')); ?>"><i class="mdi mdi-reply"></i> <?php echo e(__('Return Home')); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?><?php /**PATH /Users/kevindupas/Documents/Dev/Tinker/resources/views/projects/taskCreate.blade.php ENDPATH**/ ?>